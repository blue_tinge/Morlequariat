#ifndef __MUSIC__
#define __MUSIC__

#include <string>

namespace music {

  void switchTo(int);
  
  void playSound(std::string, double pitch = 1);

  /*
    addCharacterTrack(int);
    removeCharacterTrack(int);
  */
  
  void cleanup();

}

#endif