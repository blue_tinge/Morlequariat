#include "buttons.h"


namespace buttons {
	sf::Texture selectionsTexture;
	sf::Sprite stamp(selectionsTexture);
	
	void stitchLargeTargetTexture(int radius, sf::RenderTexture* rt) {
		
		int texsize = TILESIZE*(radius*2 + 1);
		
		// Default
		stamp.setTextureRect(sf::IntRect(0, 0, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, 0);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(TILESIZE/3, 0, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(texsize - 2*TILESIZE/3, radius*TILESIZE);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, TILESIZE/3, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, texsize - 2*TILESIZE/3);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 0, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(0, radius*TILESIZE);
		rt->draw(stamp);
		
		// Highlighted
		stamp.setTextureRect(sf::IntRect(0, TILESIZE, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(texsize + radius*TILESIZE, 0);
		rt->draw(stamp);
		
		stamp.setTextureRect(sf::IntRect(TILESIZE/3, TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(2*texsize - 2*TILESIZE/3, radius*TILESIZE);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 4*TILESIZE/3, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(texsize + radius*TILESIZE, texsize - 2*TILESIZE/3);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(texsize, radius*TILESIZE);
		rt->draw(stamp);
		
		// Pressed
		stamp.setTextureRect(sf::IntRect(0, 2*TILESIZE, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, texsize);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(TILESIZE/3, 2*TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(texsize - 2*TILESIZE/3, texsize + radius*TILESIZE);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 7*TILESIZE/3, TILESIZE, 2*TILESIZE/3));
		stamp.setPosition(radius*TILESIZE, 2*texsize - 2*TILESIZE/3);
		rt->draw(stamp);

		stamp.setTextureRect(sf::IntRect(0, 2*TILESIZE, 2*TILESIZE/3, TILESIZE));
		stamp.setPosition(0, texsize + radius*TILESIZE);
		rt->draw(stamp);
		
		rt->display();
	}
	
	std::map<int, sf::RenderTexture*> largeTargetTextures;
}

Button::Button(sf::IntRect defaultSlice, sf::IntRect highlightSlice, sf::IntRect pressedSlice, sf::IntRect disabledSlice, int x, int y, const std::string& textureFilename) : _defaultSlice(defaultSlice), _highlightSlice(highlightSlice), _pressedSlice(pressedSlice), _disabledSlice(disabledSlice), _x(x), _y(y) {
	
	static std::map<std::string, sf::Texture*> textureCache;
	
	if(textureCache.find(textureFilename) == textureCache.end()){
		sf::Texture* tex = new sf::Texture;
		tex->setSmooth(false);
		tex->loadFromFile(textureFilename);
		tex->setSmooth(false);
		textureCache[textureFilename] = tex;
	}
	
	sf::Texture* tex = textureCache[textureFilename];
	sprite = new sf::Sprite(*tex);
	
	sprite->setPosition(x,y);
	sprite->setTextureRect(defaultSlice);
	
	rect = sf::IntRect(x, y, defaultSlice.width, defaultSlice.height);
	
	_disabled = false;
	
}

Button::Button(int tilesetX, int tilesetY, int screenX, int screenY) : Button ( 
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,tilesetY*4*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+1)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+2)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		sf::IntRect(tilesetX*MENU_BUTTON_SIZE,(tilesetY*4+3)*MENU_BUTTON_SIZE,MENU_BUTTON_SIZE,MENU_BUTTON_SIZE),
		screenX, screenY,
		"img/buttons.png"
	) {}
	
Button::Button(int radius, int screenX, int screenY) : _x(screenX), _y(screenY) {
	
	int texsize = TILESIZE*(radius*2 + 1);
	
	buttons::largeTargetTextures[radius] = new sf::RenderTexture;
	buttons::largeTargetTextures[radius] -> create(texsize*2, texsize*2);
		
	_defaultSlice = sf::IntRect(0, 0, texsize, texsize);
	_highlightSlice = sf::IntRect(texsize, 0, texsize, texsize);
	_pressedSlice = sf::IntRect(0, texsize, texsize, texsize);
	_disabledSlice = sf::IntRect(texsize, texsize, texsize, texsize);
	
	// Stitch together the button texture
	buttons::stitchLargeTargetTexture(radius, buttons::largeTargetTextures[radius]);
	
	sprite = new sf::Sprite(buttons::largeTargetTextures[radius]->getTexture());
	sprite->setPosition(_x, _y);
	
	rect = sf::IntRect(_x, _y, texsize, texsize);
	
	_disabled = false;
	reset();
}

sf::Sprite* Button::getSprite() {
	//sprite->setTextureRect(_disabled ? _disabledSlice : _highlight ? _highlightSlice : _defaultSlice);
	return sprite;
}

void Button::highlight(){
	if(!_disabled && !_pressed) sprite->setTextureRect(_highlightSlice);
}
void Button::depress(){
	if(!_disabled) {
		sprite->setTextureRect(_pressedSlice);
		_pressed = true;
	}
}
void Button::reset(){
	sprite->setTextureRect(_disabled ? _disabledSlice : _defaultSlice);
	_pressed = false;
}
void Button::disable(bool disable){
	//if(disable) sprite->setTextureRect(_disabledSlice);
	_disabled = disable;
	reset();
}
