

#ifndef __BUTTONS__
#define __BUTTONS__

#include <utility>
#include <functional>
#include <vector>
#include <unordered_set>

#include "core.h"
//#include "characters.h"
//#include "weapons.h"

#include <SFML/Graphics.hpp>

#define MENU_BUTTON_SIZE 64

#define SELECT_EQUIPMENT 0x80
#define SELECT_WEAPON (SELECT_EQUIPMENT * 2)
#define SELECT_GROUND (SELECT_EQUIPMENT * 3)
#define INVENTORY_ROW 3

#define GROUNDSPACE 4

class Enemy;

class Button {
	public:
		Button(sf::IntRect, sf::IntRect, sf::IntRect, sf::IntRect, int, int, const std::string&);
		Button(int, int, int, int);
		Button(int, int, int);
	
		sf::IntRect rect;
		
		void highlight();
		void depress();
		void reset();		
		
		void disable(bool);
		
		void trigger();
				
		sf::Sprite* getSprite();
		
		bool isDisabled() const {return _disabled;};
		
		std::function<void()> effect = [](){};
		
	private:
		sf::Sprite* sprite;
		
		//void (*effect)();
		bool _disabled;
		bool _pressed;
		
		int _x;
		int _y;
		
		sf::IntRect _defaultSlice;
		sf::IntRect _highlightSlice;
		sf::IntRect _disabledSlice;
		sf::IntRect _pressedSlice;
		
		//static std::map<std::string, sf::Texture*> textureCache;
};


namespace buttons {

	const std::unordered_set<Button*>& listButtons();
	
	void setInteractive(bool);
	
	void resetCharactersMenu();
	void hideTurnMenu();
	void hideTargets();
	void hideGroundItems();
	void hideInventory();
	void hideSpiritMenu();
	//void setTargetResult(Targetable*);
	void target(int, int);
	//void setTargetResult(void(*)(int,int));
	void allowTargets(const std::vector<std::pair<int,int> >&);
	void allowLargeTarget(int);
	void showDialogueOptionButtons(int);
	void hideDialogueOptionButtons();
	
	void selectCharacter(int);
	void selectItem(int);
	void selectWeapon(int);
	void selectEquipment(int);
	void selectGroundItem(int);
	void selectSpell(int);
	
	void setVisible(Button*, bool);
	
	void onStatus();
	void onMove();
	void onAttack();
	void onSpirit();
	void onItem();
	void onWait();
	void onLook();
	
	void onCancel();
	void onEndTurn();
	void onRespawn();
	void onReturn();
		
	void onItemUse();
	void onItemTake();
	void onItemGive();
	void onItemDrop();
	void onItemEquip();
	void onItemThrow();
	void onItemRemove();
	
	void incrementGroundScroll(int);
	
	void onCommitAttack(int);
	
	void onActionComplete();	
	
	void onEnemyLook(Enemy*);
	
	void setTravelMode(bool);
	bool isTravelMode();
	
	void onNumberKey(int);
	void onArrowKey(direction);
	void onItemHotkey();
	void onSpiritHotkey();
	void onSpacebar();
	void onNextCharacter();
	
	void toNextCharacter(bool);
	
	void init();
	
	int getSelectedCharacter();
	int getSelectedItem();
	int getGroundScrollOffset();

	void addPlayerCharacter();
	
	void showDeathScreen();
	
	extern sf::Texture selectionsTexture;
	
	void setCombat(bool);
}

/*class CharacterButton: public Button {
	public:
		CharacterButton(PlayerCharacter pc);
	
};*/

#endif