#ifndef STATUS
#define STATUS

#include <vector>
#include <string>

enum conditionId {
	
	spdDn, rngDn, spConsUp,
	phAtkDn, hlAtkDn, fiAtkDn, fsAtkDn, pnAtkDn, crAtkDn,
	phDefDn, hlDefDn, fiDefDn, fsDefDn, pnDefDn, crDefDn,
	poisoned, burning, immob, cursed, mundane,
	
	posNegSep, // separator between positive and negative status conditions
	
	spdUp, rngUp, spConsDn,
	phAtkUp, hlAtkUp, fiAtkUp, fsAtkUp, pnAtkUp, crAtkUp,
	phDefUp, hlDefUp, fiDefUp, fsDefUp, pnDefUp, crDefUp,
	barrier, spicy, minty, regen, levit, timely, phoenix
};

struct statusCondition {
	conditionId id;
	int duration;
	int intensity;
	
	///statusCondition(int _id, int _duration) : id(_id), duration(_duration) {};
};

namespace status {
  extern const std::vector<std::string> names;
	//void init();
	//std::string getStatusName(int);
}

#endif
