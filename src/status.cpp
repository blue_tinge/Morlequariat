#include <fstream>
#include <vector>

#include "status.h"

namespace {
    
    std::vector<std::string> initNames() {
        std::vector<std::string> names;
        std::ifstream infile("misc/status.asdf");
        std::string line;
        while(getline(infile, line)){
            names.push_back(line);
        }
        return names;
    }
    
}

namespace status {

	const std::vector<std::string> names = initNames();

	//void init(){
		//ifstream infile("misc/status.asdf");
		//string line;
		//while(getline(infile, line)){
		//	names.push_back(line);
		//}
	//}
	
	//std::string getStatusName(int id){
	//	return names[id];
	//}

}

