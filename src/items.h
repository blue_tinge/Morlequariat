#ifndef __ITEMS__
#define __ITEMS__

#include <unordered_map> // TODO actually use this
#include <vector>
#include <SFML/Graphics.hpp>

// item categories
#define ICAT_default 0
#define ICAT_potion 1

#define ITEM_IMGSIZE 64

class StattedCharacter;

enum itemEffectType {
	defaultEffect, heal, spirit, damage, cure, statusEffect, terrain, atmosphere, spellLike, equipment, misc, reusableDamage
};

class ItemOrWeapon {
	public:
		sf::Sprite* getSprite() const { return sprite; }
		const std::string getName() const { return name; }
		const std::string getFlavor() const { return flavor; }
		virtual int getID() const = 0;
		virtual bool isWeapon() const = 0;
		
		sf::Sprite* sprite;
		std::string name;
		std::string flavor;
};

struct itemEffect {
	itemEffectType effect;
	int* params;
	//int param1;
	//int param2;
};


class item : public ItemOrWeapon {
	public:
		itemEffect useEffect;
		itemEffect throwEffect;
		const int id;
		int category;
		bool isWeapon() const override { return false; }
		int getID() const override { return id; }
		
		item(int _id) : id(_id) {}
};

namespace items {
	void init();
	item* getItem(int);
	
	void throwItemAtLocation(item*, int,int);
	bool useItemOnCharacter(itemEffect, StattedCharacter*);
	
	bool isPermanent(item* const);
	
	int itemCategoryToSpriteIndex(int);
}

#endif
