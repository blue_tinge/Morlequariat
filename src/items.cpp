#include "items.h"
#include "characters.h"
#include "display.h"
//#include "weapons.h"
#include "world.h"
//#include "status.h"

#include <fstream>
#include <sstream>

#include <iostream>
using namespace std;

namespace items {
	
	std::vector<item*> allItems;
	item* getItem(int id){
		return allItems[id];
	}

	sf::Texture* itemsTexture;

	itemEffect stringToEffect(std::string s){
		stringstream ss;
		ss.str(s);
		itemEffect output;
		char type;
		ss >> type;
		int params;
		switch(type){
			case '-': output.effect = itemEffectType::defaultEffect; output.params = 0; return output;
			case 'h': params = 2; output.effect = itemEffectType::heal; break;
			case 's': params = 2; output.effect = itemEffectType::spirit; break;
			case 'c': params = 1; output.effect = itemEffectType::cure; break;
			case 'd': params = 2; output.effect = itemEffectType::damage; break;
			case 'D': params = 2; output.effect = itemEffectType::reusableDamage; break;
			case 'e': params = 3; output.effect = itemEffectType::statusEffect; break;
			case 't': params = 3; output.effect = itemEffectType::terrain; break;
			case 'a': params = 2; output.effect = itemEffectType::atmosphere; break;
			case 'S': params = 2; output.effect = itemEffectType::spellLike; break;
			case 'E': {
				output.effect = itemEffectType::equipment;
				output.params = new int[1];
				return output; // Fill the parameter with object id
			}
			case 'm': params = 2; output.effect = itemEffectType::misc; break;
			default: std::cout << "String [" << s << "] results in unknown type " << type << std::endl;
		}
		
		output.params = new int[params];
		for(int i=0; i<params;++i) ss >> output.params[i];
		return output;
	}

	void init(){
		itemsTexture = new sf::Texture;
		itemsTexture->loadFromFile("img/all_items.png");
		
		int tilesetWidth = itemsTexture->getSize().x / ITEM_IMGSIZE;
		//int tilesetHeight = itemsTexture->getSize().y / ITEM_IMGSIZE;
		
		//Adhoc Serialized Data Format
		ifstream itemFile("misc/items.asdf");
		string line;
		stringstream lineStream;
		int id = 0;
		while(getline(itemFile, line)){
			lineStream.str(line);
			lineStream.clear();
			// Line format is {name}\t{flavor}\t{category}\t{useEffect}\t{throwEffect}
			
			item* i = new item(id);
			
			getline(lineStream, i->name, '\t');
			getline(lineStream, i->flavor, '\t');
			string temp;
			
			getline(lineStream, temp, '\t');
			i->category = stoi(temp);
			
			getline(lineStream, temp, '\t');
			i->useEffect = stringToEffect(temp);
			if(i->useEffect.effect == itemEffectType::equipment) i->useEffect.params[0] = id;
			// This is easier so that useItemOnCharacter can still take effects instead of items
			
			getline(lineStream, temp, '\t');
			i->throwEffect = stringToEffect(temp);
			
			i->sprite = new sf::Sprite;
			i->sprite->setTexture(*itemsTexture);
			i->sprite->setTextureRect(sf::IntRect(id%tilesetWidth * ITEM_IMGSIZE, id/tilesetWidth * ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE));
			
			allItems.push_back(i);
			++id;
		}
	}

	bool useItemOnCharacter(itemEffect ie, StattedCharacter* c) {
		// Returns true if the item is consumed
		
		PlayerCharacter* pc = dynamic_cast<PlayerCharacter*>(c);
		
		switch(ie.effect){
			case heal:
				std::cout << "item is heal" << std::endl;
				if(ie.params[1]){
					if(pc) display::sparkleHealthMeter(pc);
					c->takeDamage(ie.params[0], damage_type::holy);
				} else {
					// animation on sprite of healing
					c->heal(ie.params[0]);
				}
				return true;
			case spirit:
				std::cout << "item is spirit" << std::endl;
				if(!pc) return false;
				if(ie.params[1]) pc->setSpirit(pc->getSpirit() + ie.params[0]);
				else if(pc->getSpirit() + ie.params[0] > pc->getMaxSpirit()){
					pc->setSpirit(pc->getMaxSpirit());
				} else {
					pc->setSpirit(pc->getSpirit() + ie.params[0]);
				}
				display::sparkleSpiritMeter(pc);
				return true;
			case damage:
			case reusableDamage:
				std::cout << "item is damage" << std::endl;
				c->takeDamage(ie.params[1], (damage_type)ie.params[0]);
				return ie.effect == damage;
			// case cure: choose certain status conditions based on params and remove them
			case statusEffect:
				std::cout << "item is condition" << std::endl;
				c->addCondition((conditionId)ie.params[0], ie.params[1], ie.params[2]);
				return true;
			// case terrain: apply terrain effect params[0] in square of radius params[1] centered on character's xy
			// case atmosphere: apply weather effect params[0] for duration params[1]
			case spellLike:
				std::cout << "item is spell-like" << std::endl;
				if(!pc) return false;
				if(pc->getSpirit() > ie.params[1]) {
					//pc->castSpell(ie.params[0]);
					pc->setSpirit(pc->getSpirit() - ie.params[1]);
				}
				return false;
			case equipment:
				if(!pc) return false;
				return pc->equip(ie.params[0]);
			case misc:
				std::cout << "item is other" << std::endl;
				switch(ie.params[0]){
					// ...
				}
				break;
			default:
				std::cout << "item is...? " << ie.effect << std::endl;
		}
		return false;
	}
	
	bool isPermanent(item* const i) {
		return i->useEffect.effect == itemEffectType::equipment
		    || i->useEffect.effect == itemEffectType::spellLike
			|| i->throwEffect.effect == itemEffectType::reusableDamage
			;
	}

	int itemCategoryToSpriteIndex(int category) {
		switch(category) {
			case 1: return 2; // bottle
			case 2: return 3; // plant
			case 4: return 5; // counterweight
			case 5: return 6; // key
			default: return 1; // default item
		}
	}

	void throwItemAtLocation(item* i, int x, int y) {
		if(StattedCharacter* c = world::getCharacterAt(x,y)){
			if(useItemOnCharacter(i->throwEffect, c)) return;
		}
		switch(i->throwEffect.effect){
			case damage:
			case reusableDamage:
				std::cout << "item is damage" << std::endl;
				world::damageAtTile(x, y, i->throwEffect.params[1], (damage_type)i->throwEffect.params[0]);
			// spellLike and atmosphere won't be on throw effects
			// If there's no character: heal, spirit, statusEffect, and cure won't do anything
			// (probably), and damage will have very limited uses
			// case terrain:
			default: world::putItem(x,y,i);
			
		}
	}
}
