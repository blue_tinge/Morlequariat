#include "characters.h"
#include "animations.h"
#include "world.h"
#include "triggers.h"
#include "timekeeping.h"
#include <stdlib.h> // abs
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>

using namespace std;

void Enemy::specificAddCondition(statusCondition& sc) { }
bool Enemy::specificConditionForTurn(statusCondition& sc) { return false; }
bool Enemy::specificRemoveCondition(statusCondition& sc) { return false; }

struct enemyTemplate {
	int hp;
	int speed;
	int range;
	int attack;
	damage_type attackType;
	int damageTaken[NUM_DAMAGE_TYPES];
	string name;
	int flags;
};

vector<enemyTemplate> templates;

const sf::Texture& getEnemyTexture(const int templateId) {
	static map<int, sf::Texture> enemyTextures;
	if (enemyTextures.find(templateId) == enemyTextures.end()) {
		enemyTextures.emplace(templateId, sf::Texture());
		enemyTextures[templateId].loadFromFile("img/enemy/"+to_string(templateId)+".png");
	}
	return enemyTextures[templateId];
}

void characters::initEnemies(){
	ifstream enemyFile("misc/enemies.asdf");
	string line;
	stringstream lineStream;
	while(getline(enemyFile, line)){
		lineStream.str(line);
		lineStream.clear();
		
		enemyTemplate et;
		
		getline(lineStream, et.name, '\t');
		
		string temp;
		getline(lineStream, temp, '\t');
		et.flags = stoi(temp);
		getline(lineStream, temp, '\t');
		et.hp = stoi(temp);
		getline(lineStream, temp, '\t');
		et.speed = stoi(temp);
		getline(lineStream, temp, '\t');
		et.range = stoi(temp);
		getline(lineStream, temp, '\t');
		et.attack = stoi(temp);
		getline(lineStream, temp, '\t');
		et.attackType = (damage_type)stoi(temp);
		
		for(int i=0; i<NUM_DAMAGE_TYPES; ++i){
			getline(lineStream, temp, ' ');
			et.damageTaken[i] = stoi(temp);
		}
		
		templates.push_back(et);
	}
}

Enemy::Enemy(int _x, int _y, int templateId, int _drop){
	if(templateId >= templates.size()){
		cerr << "No such enemy " << templateId << " (tried to create at " << _x << ", " << _y << ")" << endl;
		templateId = 0;
	}
	enemyTemplate templ = templates[templateId];
	
	typeId = templateId;
	
	x=_x;
	y=_y;
	spawnX = _x;
	spawnY = _y;
	hpMax = templ.hp; //50;
	speed = templ.speed; //4;
	range = templ.range;
	flags = templ.flags;
	attackStrength = templ.attack;
	attackType = templ.attackType;
	name = templ.name;
	for(int i=0; i<NUM_DAMAGE_TYPES; ++i){
		damageTaken[i] = templ.damageTaken[i];
	}
	hp = hpMax;
	done=false;
	
	drop = _drop;

	if( flags & ENEMY_INVIS ) {
		currentAnimation = NULL;
	} else {
		const sf::Texture& tex = getEnemyTexture(templateId);
		sprite.setTexture(tex);
		currentAnimation = characters::createLinearAnimation(0,0,tex.getSize().x/48,0,0,48,48);
	}
}

void Enemy::onDeath(){
	triggers::processGeographicTrigger(triggers::input_type::enemyDie, spawnX, spawnY, typeId);
	triggers::processGeographicTrigger(triggers::input_type::enemyExit, x, y, typeId);
	if(drop >= 0) world::putItem(x, y, items::getItem(drop));
}

void Enemy::attack(int x, int y){
	StattedCharacter* target = world::getCharacterAt(x,y);
	int deltax = this->x - x;
	int deltay = this->y - y;
	if(deltax * deltax + deltay * deltay > 1) {
		int attackStrength = this->attackStrength;
		damage_type attackType = this->attackType;
		Enemy* _this = this;
		animations::animateProjectile(this->x, this->y, x, y, attackType, [target, attackStrength, attackType, _this](){
			if(target){
				target->takeDamage(attackStrength, attackType);
			}
			_this->setDone();
		});
	} else {
		if(target){
			target->takeDamage(attackStrength, attackType);
			setDone();
		} else {
			std::cout << "Nothing to hit at " << x << ", " << y << std::endl;
		}
	}
}

void Enemy::moveTo(int _x, int _y){
	
	int oldX = x;
	int oldY = y;
	
	triggers::processPerimeterTrigger(triggers::input_type::enemyExit, x, y, _x, _y, typeId);
	
	setXY(_x, _y);
	
	triggers::processPerimeterTrigger(triggers::input_type::enemyEnter, x, y, oldX, oldY, typeId);

	if(world::getTerrain(x,y) & FLAG_CUSTOM) {
		world::handleCustomTerrain(world::getTerrain(x,y), this);
	}
}

void Enemy::takeStep(){
	if(hp < 0) return;
	
	int minDistance = 65536; // TODO get a better arbitrarily large number
	//PlayerCharacter* closestChar = NULL;
	int targetX = 0;
	int targetY = 0;
	
	for(int i=0; i < characters::playable.size(); ++i){
		PlayerCharacter* pc = characters::playable[i];
		if (pc->getHealth() == 0) continue;
		
		// terrible hack for dungeon 1 end boss
		const int BUTTON_X = 6;
		const int BUTTON_Y = 6;
		if(typeId == 11 && world::getTileImage(pc->getX(), pc->getY()) == 0x5e && std::abs(x - BUTTON_X) + std::abs(y - BUTTON_Y) <= stepsLeft) {
			minDistance = 65536; // don't attack
			targetX = BUTTON_X;
			targetY = BUTTON_Y;
			break;
		}
		
		int distance = std::abs(x - pc->getX()) + std::abs(y - pc->getY());
		if (distance < minDistance) {
			minDistance = distance;
			//closestChar = pc;
			targetX = pc->getX();
			targetY = pc->getY();
			
		}
	}
		
  // hack for enemies that move away after attacking
  int tempRange = range;
  if((flags & ENEMY_MOVE_AFTER_ATTACK) && hasAttacked){
    tempRange = speed - 1; //temporarily set range so that it moves away (close enough to attack and move away next turn)
  }
  
	if(minDistance == tempRange) {
    if (flags & ENEMY_MOVE_AFTER_ATTACK){
      --stepsLeft;
      int frameDelay = currentAnimation ? currentAnimation->frames : 4;
      if(stepsLeft > 0 && hp > 0) timekeeping::schedule([this](){this->takeStep();}, frameDelay);
    }
    else stepsLeft = 0;
		if (!hasAttacked) {
			attack(targetX, targetY);
			hasAttacked = true;
		}
	} else if(stepsLeft > 0) {
		int deltaX = x - targetX;
		int deltaY = y - targetY;
				
		std::vector<std::pair<int, int>> options;

		std::pair<int, int> xStep;
		std::pair<int, int> yStep;
		
		if((deltaX > 0 && minDistance > tempRange) || (deltaX < 0 && minDistance < tempRange)){
			xStep = std::make_pair(x-1, y);
		} else {
			xStep = std::make_pair(x+1, y);
		}
		
		if((deltaY > 0 && minDistance > tempRange) || (deltaY < 0 && minDistance < tempRange)){
			yStep = std::make_pair(x, y-1);
		} else {
			yStep = std::make_pair(x, y+1);
		}
				
		if(std::abs(deltaX) > std::abs(deltaY)){
			options.push_back(xStep);
			if(deltaY != 0) options.push_back(yStep);
		} else {
			options.push_back(yStep);
			if(deltaX != 0) options.push_back(xStep);
		}
		
		if(minDistance < tempRange) {
			if(deltaX == 0) {
				options.push_back(std::make_pair(x+1, y));
				options.push_back(std::make_pair(x-1, y));
			}
			if(deltaY == 0) {
				options.push_back(std::make_pair(x, y+1));
				options.push_back(std::make_pair(x, y-1));
			}
		}
		
		for(int i=0; i<options.size(); ++i) {
			int terrain = world::getTerrain(options[i].first, options[i].second);
			if( !(
				world::blockedOrOccupied(options[i].first, options[i].second) || // can't move there if it's blocked or occupied
				((terrain & FLAG_NOSTAND) && !(flags & ENEMY_FLY)) || // can't move there if there's a pit and it doesn't fly
				((flags & ENEMY_AVOID_SPECIAL) && (terrain & FLAG_CUSTOM)) || // can't move there if there's a custom effect it doesn't like
				(options[i].second < y && terrain & FLAG_ONEWAY) // can't move up a cliff
			)){
				moveTo(options[i].first, options[i].second);
				break;
			}
		}
		// TODO if can't move away, attack...?
		
		--stepsLeft;
    
    
		int frameDelay = currentAnimation ? currentAnimation->frames : 4;
		if(hp > 0) timekeeping::schedule([this](){this->takeStep();}, frameDelay);
	} else done = true;
}
void Enemy::specificBeginTurn(){
	
	stepsLeft = speed;
	hasAttacked = false;
	done = false;
	takeStep();
	
}

bool Enemy::isTouchingGround() const {
	return ! (flags & ENEMY_FLY);
}
	
	/*if(x%2 && y%2) --x;
	else if(x%2) ++y;
	else if(y%2) --y;
	else ++x;*/

	/*
		Eventual algorithm:
		Identify spaces that are close enough to hit some player character.
		Sort those by graph-theoretical distance to closest player and remove all but maximum.
		Sort those by graph-theoretical distance to self and remove all but minimum.
		Choose one arbitrarily.
		Move a number of steps equal to speed along that path.
	*/

