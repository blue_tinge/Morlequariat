#include "core.h"
#include "animations.h"
#include "timekeeping.h"
//#include "display.h"

#include <memory>

#define SPARKLEFRAMES 11

namespace display {
	void addProjectile(std::shared_ptr<animations::Projectile>);
}

namespace animations {

	sf::Texture damageTexture;
	sf::Texture thrownItemTexture;
	
	void init(){
		// initialize all standard animations here
		
		damageTexture.loadFromFile("img/damage.png");
		thrownItemTexture.loadFromFile("img/item_overworld.png");
		
		for(int i=0; i<6; ++i){ // number of damage types should be defined somewhere
			takeDamage[i].frames = 5;
			takeDamage[i].loop = false;
			takeDamage[i].texture = &damageTexture;
			takeDamage[i].frameRects = new sf::IntRect[takeDamage[i].frames];
			takeDamage[i].speed = 1;
			for(int j=0; j<takeDamage[i].frames; ++j){
				takeDamage[i].frameRects[j] = sf::IntRect(j*TILESIZE, i*TILESIZE, TILESIZE, TILESIZE);
			}
		}
		
		
		airBuff.frames = 5;
		airBuff.loop = false;
		airBuff.texture = &damageTexture;
		airBuff.frameRects = new sf::IntRect[airBuff.frames];
		airBuff.speed = 1;
		for(int j=0; j<airBuff.frames; ++j){
			airBuff.frameRects[j] = sf::IntRect(j*TILESIZE, 7*TILESIZE, TILESIZE, TILESIZE);
		}
		
		sf::Texture* sparkleTexture = new sf::Texture;
		sparkleTexture->loadFromFile("img/sparkle.png");
		
		gainHealth.frames = SPARKLEFRAMES;
		gainHealth.loop = false;
		gainHealth.speed = 1;
		gainHealth.texture = sparkleTexture;
		gainHealth.frameRects = new sf::IntRect[SPARKLEFRAMES];
		
		gainSpirit.frames = SPARKLEFRAMES;
		gainSpirit.loop = false;
		gainSpirit.speed = 1;
		gainSpirit.texture = sparkleTexture;
		gainSpirit.frameRects = new sf::IntRect[SPARKLEFRAMES];
		
		for(int i=0; i<SPARKLEFRAMES; ++i){
			gainHealth.frameRects[i] = sf::IntRect(0, 12*i, 101, 12);
			gainSpirit.frameRects[i] = sf::IntRect(101, 12*i, 101, 12);
		}
		
	}
		
	animation takeDamage[NUM_DAMAGE_TYPES]; // TODO add more damage types
	
	animation gainHealth;
	animation gainSpirit;
	animation airBuff;

	AnimationInstance::AnimationInstance(int x, int y, bool t, animations::animation* _a) : offsetX(x), offsetY(y), atTile(t){
		a = _a;
		sprite.setTexture(*a->texture);
		sprite.setPosition(x,y);
		start = timekeeping::currentTick();
	}

	sf::Sprite* AnimationInstance::update(){
		int now = timekeeping::currentTick();
		if((now - start)/a->speed >= a->frames) {
			if(!a->loop) return NULL;
			else start = now;
		}
		sf::IntRect ir = a->frameRects[(now - start)/a->speed];
		sprite.setTextureRect(ir);
		return &sprite; 
	}
	
	void animateProjectile(int startX, int startY, int endX, int endY, damage_type dt, std::function<void()> callback) {
		std::shared_ptr<Projectile> projectile {
			new Projectile(startX, startY, endX, endY, damageTexture, TILESIZE*5, TILESIZE*dt)
		};
		display::addProjectile(projectile);
		
		timekeeping::schedule(callback, PROJECTILE_FRAMES);
	}
	
	void animateItemProjectile(int startX, int startY, int endX, int endY, int itemCategory, std::function<void()> callback) {
		std::shared_ptr<Projectile> projectile {
			new Projectile(startX, startY, endX, endY, thrownItemTexture,
				itemCategory%(thrownItemTexture.getSize().x/TILESIZE) * TILESIZE,
				itemCategory/(thrownItemTexture.getSize().x/TILESIZE) * TILESIZE)
		};
		display::addProjectile(projectile);
		
		timekeeping::schedule(callback, PROJECTILE_FRAMES);
	}
	
	Projectile::Projectile(int startX, int startY, int endX, int endY, sf::Texture& tex, int textureX, int textureY)
			: sourceX(startX*TILESIZE), sourceY(startY*TILESIZE), destX(endX*TILESIZE), destY(endY*TILESIZE) {
		startTime = timekeeping::currentTick();
		
		sprite.setTexture(tex);
		sprite.setTextureRect(sf::IntRect(textureX, textureY, TILESIZE, TILESIZE));
	}

	int Projectile::getCurrentX() const {
		return sourceX + (timekeeping::currentTick() - startTime) * (destX - sourceX) / PROJECTILE_FRAMES;
	}
	
	int Projectile::getCurrentY() const {
		return sourceY + (timekeeping::currentTick() - startTime) * (destY - sourceY) / PROJECTILE_FRAMES;
	}
	
	bool Projectile::isDone() const {
		return timekeeping::currentTick() - startTime > PROJECTILE_FRAMES;
	}
}

