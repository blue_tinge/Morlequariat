
#ifndef __CHARACTERS__
#define __CHARACTERS__

#include <map>
#include <vector>
#include <fstream>

#include <SFML/Graphics.hpp>

#include "core.h"
#include "weapons.h"
#include "items.h" //struct item;
#include "spells.h"
#include "status.h"

#define DEFAULTOFFSET MAX_RANGE
#define WEAPONSPACE 2
#define ITEMSPACE 12
#define EQUIPSPACE 3
#define SPELLSPACE 3

#define FRAME_DURATION 4

#define MAX_PARTY 2 // increase to 4 later TODO

#define ENEMY_FLY           1
#define ENEMY_INVIS         2
#define ENEMY_AVOID_SPECIAL 4
#define ENEMY_MOVE_AFTER_ATTACK 8

class PlayerCharacter;

namespace characters {
	void init();
	void initEnemies();
	void startPlayerTurn();
	
	extern std::vector<PlayerCharacter*> playable;
	
	struct animation {
		int frames;
		animation* next;
		sf::IntRect* frameRects;
		sf::Vector2i* offsets;
		int frameDuration;
	};
	animation* createLinearAnimation(int textureOffsetX, int textureOffsetY, int frames, int deltaX, int deltaY, int width, int height, animation* next=NULL, int durationMultiplier=1);
	
	void loadSave(int);
	
	bool anyCharHasItem(int);
	void removeOneItem(int);
	
	void addNextPlayable(int x, int y);
}

class Character {
	public:
		sf::Sprite* getSprite() {return &sprite;}
		int getX() const {return x;}
		int getY() const {return y;}
		
		// The difference between the character's position on the grid and
		// the location where it should be drawn, in pixels
		int getOffsetX() const {return pixelOffsetX;}
		int getOffsetY() const {return pixelOffsetY;}
		
		void setXY(int _x, int _y){x=_x; y=_y;}
	
		virtual ~Character() = default;
	
		direction getDirection() const {return dir;}
		
		void updateAnimation();
		void setAnimation(characters::animation*, bool);
		
	protected:
		int x;
		int y;
		
		sf::Sprite sprite;
		
		direction dir;
		
		int animationStart = 0;
		characters::animation* currentAnimation;
		int pixelOffsetX;
		int pixelOffsetY;
	
};

class NPC : public Character {
	public:
		NPC(int id, int dialogueStart);
		int getId() const {return id;}
		int getDialogueStart() const {return dialogueStart;}
		void startDialogue() const;
		
		void takeSteps(direction, int);
	
	protected:
		int id;
		int dialogueStart;
		sf::Texture tex;
		
		characters::animation* idle[4];
		characters::animation* walk[4];
};

class StattedCharacter : public Character{
	public:
		
		void beginTurn();
		virtual void specificBeginTurn() = 0;
		
		void addCondition(conditionId,int,int);
		int hasStatusEffect(int) const;
		int totalStatusEffectIntensity(int) const;
		virtual void specificAddCondition(statusCondition& sc) = 0;
		
		void conditionForTurn(statusCondition& sc);
		virtual bool specificConditionForTurn(statusCondition& sc) = 0;
		
		void removeCondition(statusCondition& sc);
		virtual bool specificRemoveCondition(statusCondition& sc) = 0;
		
		virtual void moveTo(int, int) = 0;
		
		void takeDamage(int, damage_type);
		virtual void onDeath() = 0;

		virtual void heal(int);
				
		int getDamageTaken(damage_type dt) const {return damageTaken[dt];}
	
		int getSpeed() const {return speed;}
		int getMaxHealth() const {return hpMax;}
		int getHealth() const {return hp;}
		
		void setHealth(int health) { hp = health; }
		
		const std::vector<statusCondition>& getStatusEffects() const {return statusEffects;}
		
		void onPush(direction dir, int collisionDamage);
		virtual bool isTouchingGround() const = 0;
	
	protected:
		int hp;
		int hpMax;
		int speed;
		
		std::vector<statusCondition> statusEffects;
		
		int damageTaken[NUM_DAMAGE_TYPES] = {100, 100, 100, 100, 100, 100};
};

class PlayerCharacter : public StattedCharacter {
	public:
		PlayerCharacter(int);
		
		int getId() const {return id;}
	
		void setMoved(bool m) {_steps = m?0:speed;}
		void setActed(bool a);
		void skip(bool s) {_skip = s;}
		bool hasActed() const {return _acted || hp == 0;}
		int stepsLeft() const {return _steps;}
		bool isDone() const { return _skip || (hasActed() && stepsLeft() == 0); }
		void onDeath() override;
		
		void specificBeginTurn() override;
		void specificAddCondition(statusCondition& sc) override;
		bool specificConditionForTurn(statusCondition& sc) override;
		bool specificRemoveCondition(statusCondition& sc) override;
		
		void heal(int) override;
		void restoreSpirit(int);
		
		std::vector<std::pair<int, int> > getWeaponTargets() const;
		
		void setDirection(direction dir);
		bool canMoveTo(int, int) const;
		void moveTo(int, int) override;
		void attackAction(int, int, int);
		void throwItem(int, int, int);
		void useItem(int, bool immediate=false);
		
		bool takeItem(item*);
		item* popItem(int);
		
		void equipWeapon(int);
		bool takeWeapon(weapon*);
		weapon* popWeapon(int);
		
		bool equip(int);
		void unequip(int);
		
		int getRange() const{return range;}
		
		//void castSpell(int, int);
		void inflictDamage(int, int, int, damage_type);
		
		std::string getName() const {return name;}
		
		int getMaxSpirit() const {return spMax;}
		int getSpirit() const {return sp;}
		void setSpirit(int s) {sp = s;}
		int getSpiritConsumption() const {return spiritConsumption;}
		
		bool canSpendSpirit(int) const;
		void spendSpirit(int);
		void castSpell(std::shared_ptr<spell>, int, int);
		
		int getDamageDealt(damage_type dt) const {return damageDealt[dt];}
		
		// allow for potential inventory-space-affecting effects
		const std::vector<item*> getInventory() const{return items;}
		const std::vector<weapon*> getOffhandWeapons() const{return weapons;}
		weapon* getEquippedWeapon() const {return equippedWeapon;}
		//const std::vector<int> getEquipment() const {return equipment;}
		bool hasEquipment(int) const;
		const int* getEquipment() const{ return equipment; }
		
		bool itemsFull() const { return spaceLeft == 0; }
		bool weaponsFull() const { return weaponSpaceLeft == 0; }
		
		const std::vector<std::shared_ptr<spell>> getSpells() { return _spells; }
		
		void onArrowKey(direction, bool);
		
		void save() const;
		
		void loadSave();
		
		int getRoomId() const { return roomId; }
		void setRoomId(int _roomId) { roomId = _roomId; }
		
		bool isTouchingGround() const override;
		
	private:
		int id;
	
		bool _acted;
		weapon* equippedWeapon;
		int range;
		int _steps;
		bool _skip;
		// TODO maybe a struct for a statblock, with a const one for base stats and a mutable one for actual stats
		int sp;
		int spMax;
		std::string name;
		
		int weaponClass;
		std::vector<std::shared_ptr<spell>> _spells;
		
		//item* items[ITEMSPACE];
		std::vector<item*> items;
		int spaceLeft;
		//weapon* weapons[WEAPONSPACE];
		std::vector<weapon*> weapons;
		int weaponSpaceLeft;
		int equipment[EQUIPSPACE];
	
		sf::Texture tex;
		
		int attack(int, int, int, damage_type);
		int weaponAttack(int, int, int);
		
		characters::animation* idle[4];
		characters::animation* walk[4];
		characters::animation* eat;
		characters::animation* ghost;
		
		int damageDealt[NUM_DAMAGE_TYPES] = {100, 100, 100, 100, 50, 0};
		int spiritConsumption = 100;
		
		int roomId;
		//bool inTransit; // true means already moving and should ignore further key-press events until done
		//void completeStep(int _x, int _y);
};

class Enemy : public StattedCharacter {

	public:
		Enemy(int,int,int, int drop=-1);
		void takeStep();
		void attack(int x, int y);
		bool isDone() const {return hp<=0 || done;};
		void onDeath() override;
		void moveTo(int, int) override;
		void setDone() {done = true;}

		std::string getName() const {return name;}
		int getAttackStrength() const {return attackStrength;}
		damage_type getAttackType() const {return attackType;}
		int getType() const {return typeId;}
		int getRange() const {return range;}
		int getDrop() const {return drop;}
		bool hasFlags(int f) const {return (flags & f) == f;}
	
		void specificBeginTurn() override;
		void specificAddCondition(statusCondition& sc) override;
		bool specificConditionForTurn(statusCondition& sc) override;
		bool specificRemoveCondition(statusCondition& sc) override;
		
		bool isTouchingGround() const override;
		
	private:
		static sf::Texture* tex;
		
		bool hasAttacked;
		int stepsLeft;
		bool done;
		
		std::string name;
		
		int spawnX;
		int spawnY;
		
		int drop;
		
		int attackStrength;
		damage_type attackType;
		
		int range;
		
		int typeId;
		
		int flags;
};


#endif
