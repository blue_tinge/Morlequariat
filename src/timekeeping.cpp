
#include <chrono>
#include <thread>
#include <functional>

namespace buttons {
	void setInteractive(bool);
}

namespace timekeeping {
	
	namespace {
		bool exit = false;
		int fps = 32;
		unsigned int _currentTick;
	}
	
	int currentTick() {return _currentTick;}
	void stop(){exit=true;}
	
	struct scheduledFunction {
		unsigned int atTime;
		std::function<void()> f;
		scheduledFunction* next;
	};
	
	scheduledFunction* queue;

	void schedule(std::function<void()> f, int delay){
		scheduledFunction* event = new scheduledFunction;
		event->f = f;
		event->atTime = currentTick()+delay;

		if(event->atTime < queue->atTime){
			event->next = queue;
			queue = event;
		} else {
			scheduledFunction* prev = queue;
			while(prev->next && prev->next->atTime < delay+currentTick()) prev = prev->next;
			event->next = prev->next;
			prev->next = event;
		}
	}
	
	/*void scheduleLast(std::function<void()> f, int delay){
		scheduledFunction* event = new scheduledFunction;
		event->f = f;

		scheduledFunction* prev = queue;
		while(prev->next->atTime < ~0u) prev = prev->next;
		event->atTime = prev->atTime + delay;
		event->next = prev->next;
		prev->next = event;
	}*/

	void clock() {
//		cout << "THREAD STARTED" << endl;
		queue = new scheduledFunction;
		queue->next = NULL;
		queue->atTime = ~0; // max unsigned int

		_currentTick = 0;

		while(!exit) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1000 / fps));
			++_currentTick;
			//std::cout << "Next event will be at " << queue->atTime << std::endl;
			while(queue->atTime < _currentTick){
				queue->f();
				scheduledFunction* oldqueue = queue;
				queue = queue->next;
				delete oldqueue;
			}
		}
	}
}
