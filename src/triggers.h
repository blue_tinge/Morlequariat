#ifndef TRIGGERS
#define TRIGGERS

#include <vector>
#include <sstream>

using namespace std;

namespace triggers {
	
	enum input_type {
		charEnter, // x1 x2 y1 y2 charId
		charExit, // x1 x2 y1 y2 charId
		itemPut, 
		itemTake,
		enemyEnter,
		enemyExit,
		damageType,
		damageAmount,
		enemyDie,
		useItem
	};

	enum output_type{
		propagate, delay, decrement, propagateFlag,
		terrain, setTile, spawnEnemy, spawnItem, spawnWeapon, sound, animation, damage, cutscene, flags, music
	};

	struct trigger_output {
		output_type type;
		int* params;
	};
	
	class Trigger{
		public:
			void execute();
			void addOutput(trigger_output&);
		
		protected:
			std::vector<trigger_output> output;
	};
	
	class GeographicTrigger : public Trigger {
		public:
			GeographicTrigger(input_type, int, int, int, int, int);
			const input_type type;
			bool containsPoint(int, int);
			bool appropriateParam(int);
		
		private:
			int x1, x2, y1, y2, param;
	};
	
	class AggregateTrigger : public Trigger {
		public:
			virtual void takeInput(int) = 0;
			virtual void decrement() {}
            virtual ~AggregateTrigger() = default;
			virtual void setState(int) {}
			virtual int getState() const { return 0; }
		protected:
			int id;
	};
	
	class JunctionTrigger : public AggregateTrigger {
		public:
			JunctionTrigger(int _id) {id=_id;}
			void takeInput(int) override;
	};
	
	class ThresholdTrigger : public AggregateTrigger {
		public:
			ThresholdTrigger(int _id, int _t) : threshold(_t){id=_id;}
			void takeInput(int) override;
			void decrement() override;
			void setState(int newState) override { state = newState; }
			int getState() const override { return state; }
		private:
			int state = 0;
			int threshold;
			
	};
	
	class SequenceTrigger : public AggregateTrigger {
		public:
			SequenceTrigger(int _id, vector<int>& _seq) : sequence(_seq){id=_id;}
			virtual void takeInput(int) override;
			void setState(int newState) override { state = newState; }
			int getState() const override { return state; }
		protected:
			int state = 0;
			std::vector<int> sequence;
	};
	
	class ResettingSequenceTrigger : public SequenceTrigger {
		public:
			ResettingSequenceTrigger(int _id, vector<int>& _seq) : SequenceTrigger(_id, _seq){}
			virtual void takeInput(int) override;
	};
	
	class ResetTrigger : public Trigger {
		public:
			ResetTrigger(int i) : listenTo(i) {}
			const int listenTo;
	};

	void clear();

	void parseTrigger(std::stringstream&);
	
	void triggerMeta(int, int);
	
	void processGeographicTrigger(input_type, int, int, int);
	void processPerimeterTrigger(input_type, int, int, int, int, int);
	
	void initializeCachedMetaTrigger(int, int);
	const std::vector<int> listCachedMetaTriggers();
}

#endif
