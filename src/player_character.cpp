#include "characters.h"
#include "display.h"
#include "buttons.h"
#include "animations.h"
#include "world.h"
#include "triggers.h"
#include "timekeeping.h"
#include "music.h"
#include <iostream>

namespace characters {
	
	std::vector<PlayerCharacter*> allChars;
	
	std::vector<PlayerCharacter*> playable; // TODO rename this to "party"
	
	void init(){
		
		for(int i=0; i<MAX_PARTY; ++i) {
			allChars.push_back(new PlayerCharacter(i));
		}
		
		initEnemies();
	}
			
	void startPlayerTurn(){
		for(int i=0; i<playable.size(); ++i) {
			playable[i]->beginTurn();
		}
		if(!display::hasDialogue()) buttons::resetCharactersMenu();
	}
	
	void loadSave(int partySize) {
		playable.clear();
		for(int i=0; i<partySize; ++i) {
			playable.push_back(allChars[i]);
			playable[i]->loadSave();
		}
	}
	
	bool anyCharHasItem(int id) {
		for(int i=0; i<playable.size(); ++i) {
			for(int j=0; j<playable[i]->getInventory().size(); ++j) {
				if(playable[i]->getInventory()[j] && (playable[i]->getInventory()[j]->id == id)) return true;
			}
		}
		return false;
	}
	
	void removeOneItem(int id) {
		for(int i=0; i<playable.size(); ++i) {
			for(int j=0; j<playable[i]->getInventory().size(); ++j) {
				if(playable[i]->getInventory()[j] && playable[i]->getInventory()[j]->id == id) {
					playable[i]->popItem(j);
					return;
				}
			}
		}
	}

	void addNextPlayable(int x, int y) {
		PlayerCharacter* next = allChars[playable.size()];
		playable.push_back(next);
		next->setXY(x, y);
		next->setRoomId(world::getRoomId());
		buttons::addPlayerCharacter();
	}
}

PlayerCharacter::PlayerCharacter(int _id) {
	id = _id;
	
	std::ifstream input("misc/"+std::to_string(id)+".char.asdf");
	
	input >> name >> hpMax >> spMax >> speed >> range >> weaponClass;
	for(int i = 0; i < NUM_DAMAGE_TYPES; ++i) input >> damageTaken[i];
	for(int i = 0; i < NUM_DAMAGE_TYPES; ++i) input >> damageDealt[i];

	dir = _down;
	hp = hpMax;
	sp = spMax;
	spaceLeft = ITEMSPACE;
	weaponSpaceLeft = WEAPONSPACE;
	for(int i=0;i<EQUIPSPACE;++i) equipment[i]=-1;
	_steps = speed;
	_acted = false;
	_skip = false;
	items = std::vector<item*>(ITEMSPACE, NULL);
	//for(int i=0; i<ITEMSPACE; ++i) items[i] = NULL;
	//takeItem(items::getItem(0));
	
	weapons = std::vector<weapon*>(WEAPONSPACE, NULL);
	//for(int i=0; i<WEAPONSPACE; ++i) weapons[i] = NULL;
	int w;
	input >> w;
	equippedWeapon = weapons::getWeapon(w);
	
	_spells = std::vector<std::shared_ptr<spell>>(SPELLSPACE, NULL);
	int s;
	input >> s;
	_spells[0] = spells::get(s);
	input >> s;
	_spells[1] = spells::get(s);
	_spells[2] = NULL; // in the future, this should be determined by equipped weapon
	
	tex.loadFromFile("img/"+name+"_spritesheet.png");
	//tex->loadFromFile("../img/eshin_spritesheet.png");
	sprite.setTexture(tex);
	
	for(int i=0;i<4;++i){
		idle[i] = characters::createLinearAnimation(0,56*i,4,0,0,48,56);
	}
	eat = characters::createLinearAnimation(0,56*4,6,0,0,48,56, idle[0]);
	ghost = characters::createLinearAnimation(0,56*5,4,0,0,48,56);
	walk[0] = characters::createLinearAnimation(192,0,6,48, 0,48,56,idle[0]);
	walk[1] = characters::createLinearAnimation(192,53,6,0,-48,48,56,idle[1]);
	walk[2] = characters::createLinearAnimation(192,53*2,6,-48,0,48,56,idle[2]);
	walk[3] = characters::createLinearAnimation(192,53*3,6,0, 48,48,56,idle[3]);
	
	currentAnimation = idle[0];
	//sprite->setPosition(x*TILESIZE, y*TILESIZE);
	
	x = 0;
	y = 0;
}

void PlayerCharacter::loadSave() {
	//std::cout << "loading a character: save/char/"+std::to_string(id) << std::endl;
	std::ifstream saved("save/char/"+std::to_string(id));
	if(saved.good()) {
		saved >> hp >> sp >> x >> y;
		
		int read_id;
		saved >> read_id;
		equippedWeapon = weapons::getWeapon(read_id);
		
		for(int i=0; i<WEAPONSPACE; ++i) {
			saved >> read_id;
			weapons[i] = read_id >= 0 ? weapons::getWeapon(read_id) : NULL;
		}
		
		for(int i=0; i<ITEMSPACE; ++i) {
			saved >> read_id;
			items[i] = read_id >= 0 ? items::getItem(read_id) : NULL;
		}
		
		for(int i=0; i<EQUIPSPACE; ++i) {
			saved >> read_id;
			equip(read_id);
		}
		
	} else {
		// default XY
		x = 1;
		y = 8;
	}
}

void PlayerCharacter::setActed(bool a){
	if(world::isCombat() && a) {
		_acted = true;
		if(_steps == 0) buttons::toNextCharacter(false);
	} else {
		_acted = false;
	}
}

void PlayerCharacter::specificBeginTurn(){
	_skip = false;
	setMoved(getHealth() <= 0);
	setActed(getHealth() <= 0);	
}

void PlayerCharacter::specificAddCondition(statusCondition& sc) {
	switch(sc.id){
		case spdDn: speed -= sc.intensity; break;
		case rngDn: range -= sc.intensity; break;
		case phAtkDn:
		case hlAtkDn:
		case fiAtkDn:
		case fsAtkDn:
		case pnAtkDn:
		case crAtkDn:
			damageTaken[sc.id - conditionId::phAtkDn] -= sc.intensity; break;
			
		case spdUp: speed += sc.intensity; break;
		case rngUp: range += sc.intensity; break;
		case phAtkUp:
		case hlAtkUp:
		case fiAtkUp:
		case fsAtkUp:
		case pnAtkUp:
		case crAtkUp:
			damageTaken[sc.id - conditionId::phAtkUp] += sc.intensity; break;
	}
}
bool PlayerCharacter::specificConditionForTurn(statusCondition& sc) { return false; }

bool PlayerCharacter::specificRemoveCondition(statusCondition& sc) { 
	switch(sc.id){
		case spdDn: speed += sc.intensity; break;
		case rngDn: range += sc.intensity; break;
		case phAtkDn:
		case hlAtkDn:
		case fiAtkDn:
		case fsAtkDn:
		case pnAtkDn:
		case crAtkDn:
			damageTaken[sc.id - conditionId::phAtkDn] += sc.intensity; return true;
			
		case spdUp: speed -= sc.intensity; break;
		case rngUp: range -= sc.intensity; break;
		case phAtkUp:
		case hlAtkUp:
		case fiAtkUp:
		case fsAtkUp:
		case pnAtkUp:
		case crAtkUp:
			damageTaken[sc.id - conditionId::phAtkUp] -= sc.intensity; return true;
	}
	return false;
}

void PlayerCharacter::inflictDamage(int x, int y, int amt, damage_type dtype){
	StattedCharacter* target = world::getCharacterAt(x,y);
	if(target) target->takeDamage(amt * damageDealt[dtype]/100, dtype);
	else{
		world::damageAtTile(x, y, amt, dtype);
	}
	//music::playSound("1");
}

void PlayerCharacter::heal(int h){
	StattedCharacter::heal(h);
	display::sparkleHealthMeter(this);
}

void PlayerCharacter::restoreSpirit(int s){
	sp += s;
	display::sparkleSpiritMeter(this);
}

void PlayerCharacter::onDeath() {
	setActed(true);
	setMoved(true);
	setAnimation(ghost, true);
	triggers::processGeographicTrigger(triggers::input_type::charExit, x, y, id);
}

int PlayerCharacter::attack(int x, int y, int baseDamage, damage_type dtype) {
	
	inflictDamage(x,y, baseDamage, dtype);
	int nextEffect = animations::takeDamage[dtype].frames;
	
	StattedCharacter* target = world::getCharacterAt(x,y);
	
	if(int spicy = hasStatusEffect(conditionId::spicy)){
		timekeeping::schedule([this, x, y, baseDamage, spicy](){inflictDamage(x,y, baseDamage * spicy / 100, damage_type::fire);}, nextEffect);
		nextEffect += animations::takeDamage[damage_type::fire].frames;
	}
	if(int minty = hasStatusEffect(conditionId::minty)){
		timekeeping::schedule([this, x, y, baseDamage, minty](){inflictDamage(x,y, baseDamage * minty / 100, damage_type::frost);}, nextEffect);
		nextEffect += animations::takeDamage[damage_type::frost].frames;
	}
	if(target && hasEquipment(34)) { // Charm of the Empty Beetle
		restoreSpirit(baseDamage * damageDealt[dtype] * target->getDamageTaken(dtype) / 50000); // restore 1 spirit for every 5 damage
	}
	
	return nextEffect;
}

// Returns how many frames the attack animation will take
int PlayerCharacter::weaponAttack(int x, int y, int strength){
	if(strength >= MAX_COMMIT) strength = MAX_COMMIT - 1;
	if(strength < 0) strength = 0;
	
	StattedCharacter* target = world::getCharacterAt(x, y);
	damage_type dtype = equippedWeapon->effects[strength].dtype;
	
	for(int i=0; i<equippedWeapon->effects[strength].extraEffects.size(); ++i) {
		if(equippedWeapon->effects[strength].extraEffects[i].type == weaponEffectType::adapt && target) {
			damage_type alt = (damage_type)(equippedWeapon->effects[strength].extraEffects[i].params[0]);
			if(damageDealt[alt] * target->getDamageTaken(alt) > damageDealt[dtype] * target->getDamageTaken(dtype)) dtype = alt;
		}
	}
	
	int baseDamage = equippedWeapon->effects[strength].baseDamage;
	
	int nextEffect = attack(x, y, baseDamage, dtype);
	
	for(int i=0; i<equippedWeapon->effects[strength].extraEffects.size(); ++i) {
		weaponEffectType wet = equippedWeapon->effects[strength].extraEffects[i].type;
		switch(wet) {
			case push:
				if(target) {
					direction pushdir = direction::_down;
					int deltax = this->x - x;
					int deltay = this->y - y;
					if(deltax > 0 && deltax > std::abs(deltay)) {
						pushdir = direction::_left;
					} else if(deltax < 0 && -deltax > std::abs(deltay)) {
						pushdir = direction::_right;
					} else if(deltay > 0) pushdir = direction::_up;
					int collisionDamage = equippedWeapon->effects[strength].extraEffects[i].params[0];
					if (target->getHealth()) {
						timekeeping::schedule([target, pushdir, collisionDamage](){target->onPush(pushdir, collisionDamage);}, nextEffect);
						// there should be an animation of pushing here
					}
				}
				break;
			case debuff:
			case stackingDebuff:
				if(target && (
						wet == weaponEffectType::stackingDebuff ||
						!target->hasStatusEffect(equippedWeapon->effects[strength].extraEffects[i].params[0])
				) ) {
					target->addCondition(
						(conditionId)(equippedWeapon->effects[strength].extraEffects[i].params[0]),
						equippedWeapon->effects[strength].extraEffects[i].params[1],
						equippedWeapon->effects[strength].extraEffects[i].params[2]
					);
				}
			case refund: {
				int refund = equippedWeapon->effects[strength].extraEffects[i].params[0];
				if(refund >= 0x10) setActed(false);
				_steps += refund % 0x10;
				break;
			}
			case splash: {
				// This is specifically for linear 3x1 aoe. In the future, this should be expanded to allow more types
				int splashDamage = equippedWeapon->effects[strength].extraEffects[i].params[1];
				damage_type splashType = (damage_type) equippedWeapon->effects[strength].extraEffects[i].params[0];
				if(this->x == x) {
					attack(x+1, y, splashDamage, splashType);
					attack(x-1, y, splashDamage, splashType);
				} else {
					attack(x, y+1, splashDamage, splashType);
					attack(x, y-1, splashDamage, splashType);
				}
				break;
			}
		}
	}
	
	return nextEffect;
}

void PlayerCharacter::attackAction(int x, int y, int strength) {
	if(world::isCombat()) {
		_steps -= strength;
		_acted = true;
		// setActed causes control to switch to the next character if they're out of steps.
		// we don't want that because some weapons give an extra action or steps
	}
	int delay = weaponAttack(x, y, strength);
	
	timekeeping::schedule([](){buttons::onActionComplete();}, delay);
}

void PlayerCharacter::throwItem(int itemIndex, int x, int y){
	item* i = items[itemIndex];
	animations::animateItemProjectile(this->x, this->y, x, y, items::itemCategoryToSpriteIndex(i->category), [i,x,y](){
		items::throwItemAtLocation(i, x, y);
		buttons::onActionComplete();
	});
	items[itemIndex] = NULL;
	++spaceLeft;
	setActed(true);
}

void PlayerCharacter::useItem(int itemIndex, bool immediate){
	setActed(true);
	auto useItemFunction = [this, itemIndex](){
		item* i = items[itemIndex];
		if(items::useItemOnCharacter(i->useEffect, this)) items[itemIndex] = NULL;
		triggers::processGeographicTrigger(triggers::input_type::useItem, x, y, i->id);
		buttons::onActionComplete();
	};

	if(immediate) useItemFunction();
	else {
		setAnimation(eat, true);
		currentAnimation->next = idle[dir]; // stay facing the same direction when done
		timekeeping::schedule(useItemFunction, eat->frames*FRAME_DURATION);
	}
}

bool PlayerCharacter::takeItem(item* i){
	if(!spaceLeft) return false;
	int j=0;
	while(items[j]) ++j;
	items[j]=i;
	--spaceLeft;
	return true;
}

bool PlayerCharacter::takeWeapon(weapon* i){
	if(!weaponSpaceLeft) return false;
	int j=0;
	while(weapons[j]) ++j;
	weapons[j]=i;
	--weaponSpaceLeft;
	
	return true;
}


void PlayerCharacter::equipWeapon(int weaponIndex){
	weapon* temp = equippedWeapon; 
	equippedWeapon = weapons[weaponIndex];
	weapons[weaponIndex] = temp;
	setActed(true);
}

bool PlayerCharacter::equip(int equipId){
	for(int i = 0; i<EQUIPSPACE; ++i) if(equipment[i] < 0) {
		equipment[i] = equipId;
		
		switch(equipId){
			case 21: // spyglass
				range += 2;
				break;
			case 20: // protective symbol
				damageTaken[damage_type::corrupt] -= 30;
				damageDealt[damage_type::holy] += 30;
				break;
			case 19: // something speed-related
				speed += 2;
				_steps += 2;
				break;
			case 31: // amethyst ring
				spiritConsumption -= 20;
				break;
		}
		
		return true;
	}
	return false;
}

void PlayerCharacter::unequip(int index){
	if(itemsFull()) return;
	
	//takeItem(items::getItem(equipment[index]));
	switch(equipment[index]){
		case 21:
			range -= 2;
			break;
		case 20:
			damageTaken[damage_type::corrupt] += 30;
			damageDealt[damage_type::holy] -= 30;
			break;
		case 19:
			speed -= 2;
			break;
		case 31:
			spiritConsumption += 20;
			break;
	}
	
	equipment[index] = -1;
}

bool PlayerCharacter::hasEquipment(int equipId) const{
	for(int i = 0; i<EQUIPSPACE; ++i) if(equipment[i] == equipId) return true;
	return false;
}

item* PlayerCharacter::popItem(int index){
	item* i = items[index];
	items[index] = NULL;
	++spaceLeft;
	return i;
}

weapon* PlayerCharacter::popWeapon(int index){
	weapon* i = weapons[index];
	weapons[index] = NULL;
	++weaponSpaceLeft;
	return i;
}

std::vector<std::pair<int, int> > PlayerCharacter::getWeaponTargets() const {
	/*std::cout << "starting gwt" << std::endl;
	std::vector<std::pair<int, int> > targets = weapons::getTargets(equippedWeapon, range);
	std::cout << "got targets" << std::endl;
	for(int i=0; i<targets.size(); ++i){
		targets[i].first += x;
		targets[i].second += y;
		std::cout << "targetted " << targets[i].first << ", " << targets[i].second << std::endl;
	}
	// TODO line of sight?
	return targets;*/
	return weapons::getTargets(equippedWeapon, range);
}

void PlayerCharacter::setDirection(direction dir) {
	this->dir = dir;
	setAnimation(idle[dir], false);
}

void PlayerCharacter::onArrowKey(direction dir, bool isSelected){
	int newX = x;
	int newY = y;
	switch(dir){
		case _up: --newY; break;
		case _down: ++newY; break;
		case _left: --newX; break;
		case _right: ++newX; break;
	}
	if(hp == 0) {
		if(roomId == world::getRoomId() && !world::outOfBounds(newX, newY)) setXY(newX, newY);
	} else if(_steps > 0 && canMoveTo(newX, newY)) moveTo(newX,newY);
	else {
		setDirection(dir);
		if(world::blocked(newX, newY) && id == buttons::getSelectedCharacter()) world::onLook(newX, newY);
	}
}

bool PlayerCharacter::canMoveTo(int _x, int _y) const {
	if(roomId != world::getRoomId()) return false; // can't move if you're off-screen
	if((world::getTerrain(_x,_y) & FLAG_NOSTAND) && isTouchingGround()) return false;
	if(_y < y && (world::getTerrain(_x,_y) & FLAG_ONEWAY)) return false;
	return !world::blockedOrOccupied(_x,_y);
}

bool PlayerCharacter::isTouchingGround() const {
	return !(hasEquipment(28) || hasStatusEffect(conditionId::levit));
}

void PlayerCharacter::moveTo(int _x, int _y){
	if(_x < x) {
		dir = direction::_left;
	} else if(_x > x) {
		dir = direction::_right;
	} else if(_y < y) {
		dir = direction::_up;
	} else if(_y > y) {
		dir = direction::_down;
	}
	setDirection(dir);
	//setAnimation(walk[dir]);
	//currentAnimation->next = idle[dir];
	
	int startTerrain = world::getTerrain(x,y);
	
	//if(characters::playable[buttons::getSelectedCharacter()] == this) display::center(_x, _y);
	
	int oldX = x;
	int oldY = y;
	
	triggers::processPerimeterTrigger(triggers::input_type::charExit, x, y, _x, _y, id);
	setXY(_x, _y);
	triggers::processPerimeterTrigger(triggers::input_type::charEnter, x, y, oldX, oldY, id);
	if(world::isCombat()) --_steps;
	
	if(world::getTerrain(x,y) & FLAG_DOOR && !(startTerrain & FLAG_DOOR)) {
		world::processDoor(this, x,y);
	}
	
	if(world::getTerrain(x,y) & FLAG_CUSTOM) {
		world::handleCustomTerrain(world::getTerrain(x,y), this);
	}
	
	if(_acted && _steps==0) buttons::toNextCharacter(false);
}

bool PlayerCharacter::canSpendSpirit(int cost) const {
	if(hasStatusEffect(conditionId::mundane)) return false;
	cost = cost * spiritConsumption / 100;
	int availableSpirit = sp;
	if(hasEquipment(23)) availableSpirit += hp; // Bloodstone Conduit
	return cost <= availableSpirit;
}

void PlayerCharacter::spendSpirit(int spiritCost) {
	spiritCost = spiritCost * spiritConsumption / 100;
	if(spiritCost <= sp) {
		sp -= spiritCost;
	} else if(hasEquipment(23) && hp + sp >= spiritCost) {
		hp -= (spiritCost - sp);
		sp = 0;
	}
}

void PlayerCharacter::castSpell(std::shared_ptr<spell> s, int targetX, int targetY) {
	spendSpirit(s->cost);
	setActed(true);
	switch(s->id) {
		case 0: { // DASH
			int oldX = x;
			int oldY = y;
			triggers::processPerimeterTrigger(triggers::input_type::charExit, x, y, targetX, targetY, id);
			
			int deltaX = 0;
			int deltaY = 0;
			int distance = 0;
			
			// Assume either targetX == x or targetY == y
			if(targetX == x) {
				if(targetY > y) {
					distance = targetY - y;
					deltaY = 1;
					setDirection(direction::_down);
				} else {
					distance = y - targetY;
					deltaY = -1;
					setDirection(direction::_up);
				}
			} else {
				if(targetX > x) {
					distance = targetX - x;
					deltaX = 1;
					setDirection(direction::_right);
				} else {
					distance = x - targetX;
					deltaX = -1;
					setDirection(direction::_left);
				}
			}
			
			int totalDelay = 0;
			
			for(int i=0; i<distance-1; ++i) { // don't attack the space where you'll end up
				int delay = weaponAttack(x+deltaX, y+deltaY, range - distance + 1);
				if(delay > totalDelay) totalDelay = delay;
				x += deltaX;
				y += deltaY;
			}
			x += deltaX;
			y += deltaY;
			
			triggers::processPerimeterTrigger(triggers::input_type::charEnter, x, y, oldX, oldY, id);
			
			if (totalDelay == 0) buttons::onActionComplete();
			else timekeeping::schedule([](){buttons::onActionComplete();}, totalDelay);
			
			break;
		}
		case 1: { // FORBIDDEN
			break;
		}
		case 2: { // RADIANCE
			std::vector<std::pair<int, int> > targets = weapons::getTargets(range_type::radius, this->range + s->rangeMod);
			for(int i=0; i<targets.size(); ++i) {
				inflictDamage(targets[i].first+x, targets[i].second+y, 25, damage_type::holy);
			}
			inflictDamage(x, y, 25, damage_type::holy); // since self is excluded from radius
			buttons::onActionComplete();
			break;
		}
		case 3: { // BARRIER
			StattedCharacter* target = world::getCharacterAt(targetX, targetY);
			
			animations::animateProjectile(this->x, this->y, targetX, targetY, damage_type::holy, [target](){
				if(target){
					// show animation of copper shield also
					target->addCondition(conditionId::barrier, 0, 1);
				}
				buttons::onActionComplete();
			});
			break;
		}
		
	}
	
}

void PlayerCharacter::save() const {
	ofstream charStream("save/char/"+to_string(id));
	charStream << hp << " " << sp << " " << x << " " << y << endl;
	charStream << getEquippedWeapon()->id;
	for(int i=0; i<WEAPONSPACE; ++i) charStream << " " << (weapons[i] ? weapons[i]->id : -1) ;
	charStream << std::endl;
		
	for(int i=0; i<ITEMSPACE; ++i) charStream << (items[i] ? items[i]->id : -1) << " " ;
	charStream << std::endl;
	
	for(int i=0; i<EQUIPSPACE; ++i) charStream << equipment[i] << " " ;
	charStream << std::endl;
}

