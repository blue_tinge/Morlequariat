
#include <SFML/Window.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
//#include "animations.h"
//#include "characters.h"
#include <list>
#include <memory>

#define SCREENWIDTH 800
#define SCREENHEIGHT 476

#define TOTALWIDTH (SCREENWIDTH+128)
#define TOTALHEIGHT (SCREENHEIGHT+64)

#define SECTIONPAD 8

class Character;
class PlayerCharacter;
class Enemy;
namespace animations { 
	struct animation;
	class Projectile;
}

extern int globalScale;

namespace display {

	void scheduleSetTile(int x, int y, int t);
	void loadBackground(unsigned char** imgIds, unsigned char** typeIds, int width, int height, int tileset);
	void drawWindow(sf::RenderWindow& window);
	
	void setGlobalScale(int);
	int getGlobalScale();
	void updateMouse(int,int);
	
	void clearView();
	void showInventory();
	void showSpellList();
	void showSpellDescription();
	void showGroundItems();
	void showCharacterStatus();
	void showDeathScreen();
	bool isClearView();
	
	void addAnimation(int, int, animations::animation*);
	void addAnimationAtTile(int, int, animations::animation*);
	void showDamageText(int, int, int);
	void addProjectile(std::shared_ptr<animations::Projectile>);
	
	void sparkleHealthMeter(PlayerCharacter*);
	void sparkleSpiritMeter(PlayerCharacter*);
	
	void center(int x, int y); // centers on the (x,y)th tile of the grid
	void center(Character* character);

	void showDialogue(const std::string&, const std::string&, int, int voiceid = -1);
	void showDialogueOptions(int, const std::shared_ptr<std::vector<std::string> >&);
	void skipDialogue();
	void hideDialogue();
	bool hasDialogue();
	
	void showSceneImage(int);

	//void startDialogue(const std::string&);
	//void startDialogue(std::list<std::string>&);
	//void skipDialogue();
	//void advanceDialogue();
	void showEnemyStatus(Enemy*);

	void init();
	void cleanup();

}
