#ifndef __WEAPONS__
#define __WEAPONS__

#include "core.h"
#include "items.h"

//#include <SFML/Graphics.hpp>

#include <utility>
#include <vector>
#include <string>
#include <stdlib.h>

#define MAX_RANGE 6

#define MAX_COMMIT 4

enum weapon_class { blade, staff, ranged };

enum weaponEffectType {aoe, debuff, adapt, push, stackingDebuff, refund, splash };

struct weaponSpecialEffect {
	weaponEffectType type;
	int* params;
};

struct weaponEffect {
		damage_type dtype;
		int baseDamage;
		std::vector<weaponSpecialEffect> extraEffects;
};

class weapon : public ItemOrWeapon {
	public:
		int id;
		range_type rtype;
		int baseRange;
		weapon_class wclass;
		
		bool isWeapon() const override { return true; }
		int getID() const override { return id; }
	
		weaponEffect effects[4];
};

namespace weapons {
	void init();
	std::vector<std::pair<int, int> > getTargets(range_type rtype, int range);
	std::vector<std::pair<int, int> > getTargets(weapon* weapon, int rangeMod);
	weapon* getWeapon(int id);
}

#endif
