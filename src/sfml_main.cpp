
#include <thread>
#include <string>
#include <iostream>
#include <fstream>

#include "status.h"
#include "buttons.h"
#include "display.h"
#include "characters.h"
#include "world.h"
#include "animations.h"
#include "triggers.h"
#include "cutscenes.h"
#include "spells.h"
#include "music.h"
#include "timekeeping.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#ifdef NATIVE_THREADS
#include <thread>
#endif

using namespace std;
static void handleResize(sf::RenderWindow& window, int w, int h);

int main(){

	cout << "START" << endl;
	items::init();
	cout << "Initialized Items" << endl;
	weapons::init();
	cout << "Initialized Weapons" << endl;
	spells::init();
	cout << "Initialized Spells" << endl;
	display::init();
	cout << "Initialized Display" << endl;
	animations::init();
	cout << "Initialized Animations" << endl;
	characters::init();
	cout << "Initialized Characters" << endl;
	world::loadSave();
	cout << "Loaded Map" << endl;
	buttons::init();
	cout << "Initialized User Interface" << endl;
	cutscenes::init();

	sf::Image icon;
	icon.loadFromFile("img/morlico.png");
	cout << "main: loaded icon" << endl;
	
	buttons::selectCharacter(0);

	sf::RenderWindow window(sf::VideoMode(TOTALWIDTH, TOTALHEIGHT), "MORLEQUARIAT");
  bool isFullscreen = false;
  
	//TODO screensize * tilesize + m1920 / 928enusize
	cout << "main: created window" << endl;
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	cout << "main: set icon" << endl;

#ifdef NATIVE_THREAD
	std::thread clockThread(timekeeping::clock);
#else
	sf::Thread clockThread(&timekeeping::clock);
	clockThread.launch();
#endif

	sf::Event event;
	while(window.isOpen()){
		
		//triggers::processTurn();
		world::checkPlayerTurn(); // Check if the enemies just finished moving //TODO THIS SHOULD NOT BE EVERY FRAME!

		while (window.pollEvent(event)) { // Might be beneficial to have an event process thread and a drawing/control thread.
			const unordered_set<Button*>& visibleButtons = buttons::listButtons();
		
			switch(event.type) {
				
				case sf::Event::Resized: {
          int w = event.size.width;
          int h = event.size.height;
          
          //if cant *quite* fill full screen but could if it was in fullscreen mode, make fullscreen
          int xRes = sf::VideoMode::getDesktopMode().width;
          int yRes = sf::VideoMode::getDesktopMode().height;
          
          //128 is arbitrary estimate to determine if maximimized in windowed mode...
          if(event.size.width + 128 >= xRes && h + 128 >= yRes &&
          (xRes / TOTALWIDTH > w / TOTALWIDTH || yRes / TOTALHEIGHT > h / TOTALHEIGHT)){
            w = xRes;
            h = yRes;
            window.create(sf::VideoMode(w, h), "MORLEQUARIAT", sf::Style::Fullscreen);
            isFullscreen = true;
          }
          
					handleResize(window,w,h);
          
					break;
				}
	
				case sf::Event::Closed: {
					window.close(); // Trigger some sort of confirmation, actually. But that can be handled later.
					timekeeping::stop();
					break;
				}

				case sf::Event::MouseMoved: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseMove.x / display::getGlobalScale(), event.mouseMove.y / display::getGlobalScale())){
							(*itr)->highlight();
						} else {
							(*itr)->reset();
						}
					}
					display::updateMouse(event.mouseMove.x, event.mouseMove.y);
					break;
				}
				
				case sf::Event::MouseButtonPressed: {
					for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
						if((*itr)->rect.contains(event.mouseButton.x / display::getGlobalScale(), event.mouseButton.y / display::getGlobalScale())){
							(*itr)->depress();
						} else {
							(*itr)->reset();
						}
					}
					break;
				}
				
				case sf::Event::MouseButtonReleased: {
					if(event.mouseButton.button == sf::Mouse::Button::Left){
						int mouseX = event.mouseButton.x / display::getGlobalScale();
						int mouseY = event.mouseButton.y / display::getGlobalScale();
						
						bool handledClick = false;
						Button* currentButton;
						for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr){
							currentButton = *itr;
							if(currentButton->rect.contains(mouseX, mouseY) && !currentButton->isDisabled()){
								currentButton->effect();
								currentButton->reset();
								currentButton->highlight();
								handledClick = true;
								break;
							}
						}
						if(!handledClick && display::hasDialogue()) {
							handledClick = true;
							if(cutscenes::isAdvanceable()) {
								cutscenes::advanceDialogue();
							} else {
								display::skipDialogue();
							}
						}
						if(!handledClick && display::isClearView()) for(int i=0; i<characters::playable.size(); ++i){
							if(characters::playable[i]->getRoomId() == world::getRoomId() && characters::playable[i]->getSprite()->getGlobalBounds().contains(mouseX, mouseY)){
								buttons::selectCharacter(i);
								handledClick = true;
								break;
							}
						}
						if(!handledClick && display::isClearView()) for(int i=0; i<world::getNPCs().size(); ++i){
							if(world::getNPCs()[i]->getSprite()->getGlobalBounds().contains(mouseX, mouseY)){
								world::getNPCs()[i]->startDialogue();
								handledClick = true;
								break;
							}
						}
					}
					break;
				}
				
				case sf::Event::KeyPressed: {
					switch(event.key.code){
						case sf::Keyboard::Num1:
						case sf::Keyboard::Num2:
						case sf::Keyboard::Num3:
						case sf::Keyboard::Num4:
						case sf::Keyboard::Num5:
						case sf::Keyboard::Num6:
						case sf::Keyboard::Num7:  buttons::onNumberKey(event.key.code - sf::Keyboard::Num1); break;
						
						case sf::Keyboard::Space: buttons::onSpacebar(); break;
						
						case sf::Keyboard::A:
						case sf::Keyboard::Left:  buttons::onArrowKey(direction::_left); break;
						case sf::Keyboard::D:
						case sf::Keyboard::Right: buttons::onArrowKey(direction::_right); break;
						case sf::Keyboard::W:
						case sf::Keyboard::Up:    buttons::onArrowKey(direction::_up); break;
						case sf::Keyboard::S:
						case sf::Keyboard::Down:  buttons::onArrowKey(direction::_down); break;
						
						case sf::Keyboard::Q:     buttons::onSpiritHotkey(); break;
						case sf::Keyboard::E:     buttons::onItemHotkey(); break;
						case sf::Keyboard::R:     buttons::onStatus(); break;
						case sf::Keyboard::F:     buttons::onLook(); break;
						
						case sf::Keyboard::Tab:   buttons::onWait(); break;
						case sf::Keyboard::Escape:buttons::onCancel(); break;
						case sf::Keyboard::C:     buttons::onNextCharacter(); break;
						case sf::Keyboard::Z: cutscenes::advanceDialogue(); break;
						case sf::Keyboard::X: display::skipDialogue(); break;
						
						case sf::Keyboard::F5:
							buttons::setInteractive(true); // debug feature for if the controls get screwed up
							break;
            case sf::Keyboard::F11:    // alternate between windowed mode and fullscreen mode
              int w = TOTALWIDTH;
              int h = TOTALHEIGHT;
              isFullscreen = !isFullscreen;   //set isFullscreen to new mode
              if (isFullscreen){
                //make fullscreen
                w = sf::VideoMode::getDesktopMode().width;
                h = sf::VideoMode::getDesktopMode().height;
                window.create(sf::VideoMode(w, h), "MORLEQUARIAT", sf::Style::Fullscreen);
              } else {
                //make small window
                window.create(sf::VideoMode(TOTALWIDTH, TOTALHEIGHT), "MORLEQUARIAT");
              }
              handleResize(window,w,h);
							break;
					}
				}
			}
		}
		
		display::drawWindow(window);
		
	}

#ifdef NATIVE_THREAD
	clockThread.join();
#else
	clockThread.wait();
#endif

	//world::freeMap();
	display::cleanup();
	music::cleanup();
	//for(int i=0; i<tilesetSize; ++i) delete tileset[i];
	//delete[] tileset;

	return 0;
}

//handle view/scale after window resize (or recreate)
static void handleResize(sf::RenderWindow& window, int w, int h){
  
  //minimum window size (possible bug if desktop cannot handle minimum resolution?)
  if (w < TOTALWIDTH || h < TOTALHEIGHT){
    w = TOTALWIDTH;
    h = TOTALHEIGHT;
    window.create(sf::VideoMode(w, h), "MORLEQUARIAT");
  }
  
  window.setView(sf::View(sf::FloatRect(0,0,w,h)));
  
  int xScale = w / TOTALWIDTH;
  int yScale = h / TOTALHEIGHT;
  
  display::setGlobalScale(xScale > yScale ? yScale : xScale);
}
