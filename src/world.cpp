#include "world.h"
#include "characters.h"
#include "animations.h"
#include "display.h"
#include "triggers.h"
#include "music.h"
#include "cutscenes.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_set>

using namespace std;

// forward declarations
namespace buttons {
	void setInteractive(bool);
	void setCombat(bool);
	void showMainTurnMenu();
	void showDeathScreen();
	void onEnemyLook(Enemy*);
	void selectCharacter(int);
}

namespace world {
		
	int width;
	int height;
	int currentId;
	
	int mostRecentCheckpoint;
	
	bool playerTurn;
	bool combat;
	
	struct door {
		int inX;
		int inY;
		int outX;
		int outY;
		int targetMap;
	};
	std::vector<door> doors;
	
	struct lookText {
		int x;
		int y;
		int index;
	};
	std::vector<lookText> lookTexts;
	std::vector<std::shared_ptr<NPC>> npcs;
	
	struct checkpoint {
		int x;
		int y;
		int charge;
	};
	
	struct room {
		int dungeonId;
		std::vector<Enemy*> enemies;
		std::map<std::pair<int,int>,std::vector<ItemOrWeapon*>*> itemPiles;
		unsigned char** types; // cache types and images because triggers can change them
		unsigned char** world_images;
		int eventFlags;
		int width;
		int height;
		std::vector<int> triggerStates;
		std::vector<checkpoint> checkpoints;
		// animations
		// triggers
	};
	std::map<int, room*> allRooms;
	room* currentRoom;
	
	unsigned char** getAllTileImages() {
		return currentRoom->world_images;
	}
	
	std::unordered_set<int> globalFlags;	
	
	struct customTerrainEffect {
		char type;
		int* params;
		bool grounded;
	};
	std::map<int, std::vector<customTerrainEffect>> customTerrains;
	
	int getRoomId() { return currentId; }
	
	void saveRoom(int mapid, room* const r) {
		// TODO in the future have multiple save files and store the index
		ofstream tiles("save/tiles/"+to_string(mapid), std::ios::binary);
		ofstream stats("save/status/"+to_string(mapid), std::ios::binary);
		
		for(int i=0; i<r->height; ++i){
			stats.write((char*) r->types[i], r->width);
			tiles.write((char*) r->world_images[i], r->width);
		}
		
		ofstream data("save/data/"+to_string(mapid));
		data << r->eventFlags << endl;
		
		for(int i=0; i<r->enemies.size(); ++i) {
			data << "e " << r->enemies[i]->getX() << " " << r->enemies[i]->getY() << " " << r->enemies[i]->getType() << " " << r->enemies[i]->getDrop() << " " << r->enemies[i]->getHealth() << std::endl;
		}
		
		for(auto itr = r->itemPiles.cbegin(); itr != r->itemPiles.cend(); ++itr) {
			std::vector<ItemOrWeapon*>* piles = itr->second;
			for(int i=0; i<piles->size(); ++i) {
				if((*piles)[i]->isWeapon()) data << "w ";
				else data << "i ";
				
				data << itr->first.first << " " << itr->first.second << " " << (*piles)[i]->getID() << std::endl;
			}
		}
		
		for(int i=0; i<r->checkpoints.size(); ++i) {
			data << "c " << r->checkpoints[i].x << " " << r->checkpoints[i].y << " " << r->checkpoints[i].charge << std::endl;
		}
		
		for(int i=0; i<r->triggerStates.size(); ++i) {
			data << "t " << i << " " << r->triggerStates[i] << std::endl;
		}
	}
	
	void deleteRoomCache(int mapid, room* r) {		
		for(int i = 0; i<r->enemies.size(); ++i) {
			delete r->enemies[i];
		}
		r->enemies.clear();
		
		for(auto itr = r->itemPiles.begin(); itr != r->itemPiles.end(); ++itr) {
			// Don't delete the contents of the vector! They're pointers to the global list of all items.
			delete itr->second;
		}
		
		for(int i=0; i < r->height; ++i) delete[] r->types[i];
		for(int i=0; i < r->height; ++i) delete[] r->world_images[i];
		delete[] r->types;
		delete[] r->world_images;
		
		delete r;
	}
	
	void resetRoom(int mapid, room* r) {
		ifstream levelDataIn("map/data/"+to_string(mapid));
		ofstream levelDataOut("save/data/"+to_string(mapid));
		stringstream lineStream;
		string line;
		getline(levelDataIn, line);
		
		int tempWidth;
		int tempHeight;
		
		lineStream.str(line);
		lineStream >> tempWidth >> tempHeight;
		// This is room-level data (bounds, music, etc). Not relevant here.
				
		// Originally, I was going to transform the old room-in-memory structure into a new room-in-memory structure.
		// But it's easier to just take everything from the room we want to keep, write it to disk, and forget the rest.
		// If the player goes back into the dungeon, read the disk again, no problem.

		levelDataOut << 0 << endl; // level flags

		while(getline(levelDataIn, line)){
			lineStream.str(line);
			lineStream.clear();
			char type;
			lineStream >> type;
			
			switch(type){
				case 'i': {
					int _;
					int id;
					lineStream >> _ >> _ >> id;
					
					// Non-permanent items (i.e. consumables) respawn.
					if(! items::isPermanent(items::getItem(id))) levelDataOut << line << endl;
					break;
				}
				// Assume that weapons aren't intrinsic to puzzles and therefore shouldn't ever reset
				case 'e': {
					levelDataOut << line << " " << -1 << endl;
					break;
				}
				
				case 'c' : {
					levelDataOut << line << endl;
					break;
				}
			}
		}
		
		// Permanent items not native to the room; the only reason why this doesn't just delete the cache
		for(auto itr = r->itemPiles.cbegin(); itr != r->itemPiles.cend(); ++itr) {
			std::vector<ItemOrWeapon*>* piles = itr->second;
			for(int i=0; i<piles->size(); ++i) {
				if((*piles)[i]->isWeapon()) {
					levelDataOut << "w" << itr->first.first << itr->first.second << (*piles)[i]->getID() << std::endl;
				} else {
					item* savedItem = static_cast<item*>((*piles)[i]);
					if(items::isPermanent(savedItem)) {
						levelDataOut << "i" << itr->first.first << itr->first.second << savedItem->id << std::endl;
					}
				}
			}
		}
		
		deleteRoomCache(mapid, r);
		
		remove(("save/tiles/"+to_string(mapid)).c_str());
		remove(("save/status/"+to_string(mapid)).c_str());
		
	}
	
	void save() {
		cout << "Saving..." << endl;
		auto itr = allRooms.begin();
		while(itr != allRooms.end()){
			std::cout << "Saving room " << itr->first << std::endl;
			if(itr->second->dungeonId && !(currentRoom->dungeonId)) {
				resetRoom(itr->first, itr->second);
				itr = allRooms.erase(itr);
			} else {
				saveRoom(itr->first, itr->second);
				++itr;
			}
		}
		for(int i=0; i<characters::playable.size(); ++i) {
			characters::playable[i]->save();
		}
		ofstream global("save/global");

		global << currentId << " ";
		global << characters::playable.size();
		
		for(int flag : globalFlags) global << " " << flag;
		
		global << endl;
		
		cout << "Saved" << endl;
	}
	

	const std::vector<Enemy*>& getEnemies(){
		return currentRoom->enemies;
	}
	
	const std::vector<std::shared_ptr<NPC>>& getNPCs(){
		return npcs;
	}
	
	void addNpc(int x, int y, int id, int ds){
		std::shared_ptr<NPC> npc = std::shared_ptr<NPC> { new NPC(id, ds) };
		npc->setXY(x, y);
		npcs.push_back(npc);
	}
	
	void loadMap(int mapid){
		
		// cache metatrigger states
		if(currentRoom) currentRoom->triggerStates = triggers::listCachedMetaTriggers();
		
		ifstream leveldata("map/data/"+to_string(mapid));
		
		stringstream lineStream;
		string line;
		getline(leveldata, line);
		lineStream.str(line);
		
		int tileset;
		int defaultMusic;
		int dungeonId;
		lineStream >> width >> height >> tileset >> defaultMusic >> dungeonId;
		
		bool cached = false;
		if(allRooms.find(mapid) != allRooms.end()){
			currentRoom = allRooms[mapid];
			cached = true;
		} else {
			
			allRooms[mapid] = new room;
			allRooms[mapid] -> dungeonId = dungeonId;
			allRooms[mapid] -> width = width;
			allRooms[mapid] -> height = height;
			currentRoom = allRooms[mapid];
			
			std::cout << "Loading map " << mapid << std::endl;;

			ifstream tiles("save/tiles/"+to_string(mapid), std::ios::binary);
			ifstream stats("save/status/"+to_string(mapid), std::ios::binary);
			
			if(!tiles.is_open()) tiles = ifstream("map/tiles/"+to_string(mapid), std::ios::binary);
			if(!stats.is_open()) stats = ifstream("map/status/"+to_string(mapid), std::ios::binary);
			
			currentRoom->types = new unsigned char*[height];
			currentRoom->world_images = new unsigned char*[height];
			for(int i=0; i<height; ++i){
				char* typeRow = new char[width];
				char* tileRow = new char[width];
				stats.read(typeRow, width);
				tiles.read(tileRow, width);
				currentRoom->types[i] = (unsigned char*) typeRow;
				currentRoom->world_images[i] = (unsigned char*) tileRow;
			}
			currentRoom->eventFlags = 0;
		}
		
		cutscenes::loadRoomDialogue(mapid);
		
		triggers::clear();
		doors.clear();
		lookTexts.clear();
		npcs.clear();
		
		for(auto itr = customTerrains.begin(); itr != customTerrains.end(); ++itr) {
			for(int i=0; i<itr->second.size(); ++i) delete[] itr->second[i].params;
		}
		customTerrains.clear();
		
		ifstream savedLevelData("save/data/"+to_string(mapid));
		
		bool saved = false;
		if(savedLevelData.is_open()) {
			saved = true;
		}
		
		vector<animations::animation*> animationTypes;

		while(getline(leveldata, line)){
			lineStream.str(line);
			lineStream.clear();
			char type;
			lineStream >> type;
	//		string type;
	//		lineStream >> type;
			switch(type){
				case 'A': {// Define animation type
					animations::animation* a = new animations::animation;
					// TODO cache textures
					// filename width height y-offset framecount
					a->texture = new sf::Texture;
					string textureFilename;
					lineStream >> textureFilename;
					a->texture->loadFromFile("img/"+textureFilename+".png");
					a->loop = true;
					int w,h,x,y;
					lineStream >> w >> h >> y >> a->frames >> a->speed;
					
					a->frameRects = new sf::IntRect[a->frames];
					std::cout << "frame count: " << a->frames << endl;
					for(int i=0;i<a->frames;++i) a->frameRects[i] = sf::IntRect(w*i, y, w, h);
					animationTypes.push_back(a);
					break;
				}
				case 'T': {// Define custom terrain effect
					int terrainId;
					lineStream >> terrainId;
					std::vector<customTerrainEffect> effects;
					while(lineStream.rdbuf()->in_avail()) {
						customTerrainEffect effect;
						lineStream >> effect.type;
						
						int params;
						switch(effect.type) {
							case 'd': params = 2; break; // damage
							case 'e': params = 2; break; // damage enemies only
							case 's': params = 3; break; // status condition
							case 'g': params = 0; break; // effect is limited to ground
              case 't': params = 1; break; // allow-throw (-1 allows all items)
						}
						effect.params = new int[params];
						for(int i=0; i<params;++i) lineStream >> effect.params[i];
						
						effects.push_back(effect);
					}
					customTerrains[terrainId] = effects;
					break;
				}
				case 'i': {// define item
					if(cached || saved) break;
					int x;
					int y;
					int id;
					lineStream >> x >> y >> id;
					putItem(x,y,items::getItem(id));
					break;
				}
				case 'w': {
					if(cached || saved) break;
					int x;
					int y;
					int id;
					lineStream >> x >> y >> id;
					putItem(x,y,weapons::getWeapon(id));
					break;
				}
				case 'e': {
					if(cached || saved) break;
					int x;
					int y;
					int id;
					int drop;
					lineStream >> x >> y >> id >> drop;
					currentRoom->enemies.push_back(new Enemy(x,y,id,drop));
					triggers::processGeographicTrigger(triggers::input_type::enemyEnter, x, y, id);
					break;
				}
				case 'n': {
					// for now, npcs probably won't need to be cached - can change later
					int x;
					int y;
					int id;
					int ds; // dialogue start
					lineStream >> x >> y >> id >> ds;
					addNpc(x, y, id, ds);
					break;
				}	
				case 'd' : {
					door d;
					lineStream >> d.inX >> d.inY >> d.targetMap >> d.outX >> d.outY;
					doors.push_back(d);
					break;
				}
				case 'a' : {
					int x,y,id;
					lineStream >> x >> y >> id;
					display::addAnimationAtTile(x,y,animationTypes[id]);
					break;
				}
				case 't': {
					triggers::parseTrigger(lineStream);
					break;
				}
				case 'l': {
					lookText lt;
					lineStream >> lt.x >> lt.y >> lt.index;
					lookTexts.push_back(lt);
					break;
				}
				case 'c': {
					if(cached || saved) break;
					checkpoint cp;
					lineStream >> cp.x >> cp.y >> cp.charge;
					currentRoom->checkpoints.push_back(cp);
					break;
				}
				default:
					cerr << "Cannot parse line in world data file (starting with character " << (int)type << "): " << line << endl;
			}
		}
		if(cached) {
			for(int i=0; i<currentRoom->triggerStates.size(); ++i) {
				triggers::initializeCachedMetaTrigger(i, currentRoom->triggerStates[i]);
			}
			
		} else if(saved) {
			cout << "Loading saved level data for level " << mapid << endl;
		
			getline(savedLevelData, line);
			currentRoom->eventFlags = stoi(line);

			while(getline(savedLevelData, line)){
				lineStream.str(line);
				lineStream.clear();
				char type;
				lineStream >> type;
				
				switch(type){
					case 'i': {// define item
						int x;
						int y;
						int id;
						lineStream >> x >> y >> id;
						putItem(x,y,items::getItem(id));
						break;
					}
					case 'w': {
						int x;
						int y;
						int id;
						lineStream >> x >> y >> id;
						putItem(x,y,weapons::getWeapon(id));
						break;
					}
					case 'e': {
						int x;
						int y;
						int id;
						int hp;
						int drop;
						lineStream >> x >> y >> id >> drop >> hp;
						Enemy* enemy = new Enemy(x,y,id);
						if(hp > 0) enemy->setHealth(hp);
						currentRoom->enemies.push_back(enemy);
						break;
					}
					case 't': {
						int id;
						int state;
						lineStream >> id >> state;
						triggers::initializeCachedMetaTrigger(id, state);
						break;
					}
					case 'c': {
						checkpoint cp;
						lineStream >> cp.x >> cp.y >> cp.charge;
						currentRoom->checkpoints.push_back(cp);
						break;
					}
					default:
						cerr << "Cannot parse line in saved room data file (starting with character " << (int)type << "): " << line << endl;
				}
			}
		}
		
		display::loadBackground(currentRoom->world_images, currentRoom->types, width, height, tileset);
		
		music::switchTo(defaultMusic);
		
		enableCombat(!currentRoom->enemies.empty());
		
		characters::startPlayerTurn();
		
		currentId = mapid;
	}
	
	void loadSave() {
		ifstream global("save/global");
		int mapid;
		int partySize = 1;
		if(global.good()) {
			global >> mapid >> partySize;
						
			int temp;
			while(!global.eof()) {
				global >> temp;
				globalFlags.insert(temp);
			}
		} else {
			mapid = 0;
		}
		currentRoom = NULL;
		characters::loadSave(partySize);
		loadMap(mapid);
		for(int i=0; i<partySize; ++i) characters::playable[i]->setRoomId(mapid);
	}
	
	void setTerrain(int x, int y, int t){
		int tOld = currentRoom->types[y][x];
		currentRoom->types[y][x] = t;
		if((tOld & FLAG_CRATE) == FLAG_CRATE) {
			if((t & FLAG_CRATE) != FLAG_CRATE) {
				display::scheduleSetTile(x, y, currentRoom->world_images[y][x]);
			}
		} else {
			if((t & FLAG_CRATE) == FLAG_CRATE) {
				display::scheduleSetTile(x, y, CRATE_TILE);
			}
		}
		if((t & FLAG_CUSTOM) == FLAG_CUSTOM && t != tOld) {
			StattedCharacter* c = getCharacterAt(x, y);
			if(c) handleCustomTerrain(t, c);
		}
	}
	
	void setTileImage(int x, int y, int t){
		currentRoom->world_images[y][x] = t;
		display::scheduleSetTile(x, y, t);
	}
	
	int getTerrain(int x, int y){
		if(x<0 || y<0 || x>=width || y>=height) return 0;
		return currentRoom->types[y][x];
	}
	
	int getTileImage(int x, int y){ // probably not recommended
		if(x<0 || y<0 || x>=width || y>=height) return 0;
		return currentRoom->world_images[y][x];
	}
	
	void enableCombat(bool _combat) {
		combat = _combat;
		buttons::setCombat(combat);
		if(!combat) {
			for(int i=0; i<characters::playable.size(); ++i) {
				characters::playable[i]->setActed(false);
				characters::playable[i]->setMoved(false);
			}
		}
	}
	
	bool isCombat() {
		return combat;
	}
	
	void damageAtTile(int x, int y, int amt, damage_type dtype) {
		if(x<0 || y<0 || x>=width || y>=height) return;
		triggers::processGeographicTrigger(triggers::input_type::damageType, x, y, dtype);
		display::addAnimationAtTile(x,y,animations::takeDamage+dtype);
		if((currentRoom->types[y][x] & FLAG_CRATE) == FLAG_CRATE) {
			setTerrain(x, y, currentRoom->types[y][x] & ~FLAG_CRATE);
		}
	}
	
	void handleCustomTerrain(int terrainId, StattedCharacter* character) {
		auto itr = customTerrains.find(terrainId);
		if(itr == customTerrains.end()) return;
		for (customTerrainEffect effect : itr->second) {
			switch(effect.type) {
				case 'e': {
					Enemy* enemy = dynamic_cast<Enemy*>(character);
					if(!enemy) break;
					// FALLTHRU
				}
				case 'd': {
					character->takeDamage(effect.params[0], (damage_type) effect.params[1]);
					break;
				}
				case 's': {
					character->addCondition((conditionId) effect.params[0], effect.params[1], effect.params[2]);
					break;
				}
				case 'g': {
					if (!character->isTouchingGround()) return;
					break;
				}
			}
		}
	}
	
	void putItem(int x, int y, ItemOrWeapon* i){
		std::cout << "putting " << i->getName() << " at " << x << ", " << y << std::endl;
		auto pair = std::make_pair(x,y);
		if(currentRoom->itemPiles.find(pair) == currentRoom->itemPiles.end()){
			currentRoom->itemPiles[pair] = new std::vector<ItemOrWeapon*>;
		}
		currentRoom->itemPiles[pair]->push_back(i);
		if(!i->isWeapon()) triggers::processGeographicTrigger(triggers::input_type::itemPut, x, y, i->getID());
	}
	void removeNthItemAt(int x, int y, int i){
		auto pair = std::make_pair(x,y);
		ItemOrWeapon* it = (*currentRoom->itemPiles[pair])[i];
		if(!it->isWeapon()) triggers::processGeographicTrigger(triggers::input_type::itemTake, x, y, it->getID());
		currentRoom->itemPiles[pair]->erase(currentRoom->itemPiles[pair]->begin() + i);
		if(currentRoom->itemPiles[pair]->size() == 0){
			std::cout << "last item taken!" << std::endl;
			delete currentRoom->itemPiles[pair];
			currentRoom->itemPiles.erase(pair);
		}
	}
	int countItemsAt(int x, int y){
		if(currentRoom->itemPiles.find(std::make_pair(x,y)) == currentRoom->itemPiles.end()) return 0;
		return currentRoom->itemPiles[std::make_pair(x,y)]->size();
	}
	const std::vector<ItemOrWeapon*>& getItemsAt(int x, int y){
		return *currentRoom->itemPiles[std::make_pair(x,y)];
	}
	
	void processDoor(PlayerCharacter* pc, int x, int y){
		std::cout << "handling door at " << x << "," << y << std::endl;
		for(int i=0; i<doors.size(); ++i){
			if(doors[i].inX == x && doors[i].inY == y){
				// have to cache these so they don't get overwritten during loadMap
				int newX = doors[i].outX;
				int newY = doors[i].outY;
				int newRoom = doors[i].targetMap;
				
				pc -> setRoomId(newRoom);
				
				for(int j=0; j<characters::playable.size(); ++j) {
					if(characters::playable[j]->getRoomId() != newRoom && characters::playable[j]->getHealth() > 0) {
						buttons::selectCharacter(j);
						return;
					}
				}
				
				// now that we've determined all the characters are indeed going into the new room
				// we position them within the new room
				
				for(int j=0; j<characters::playable.size(); ++j) {
					std::cout << "character " << j << " was " << pc->getX() << "," << pc->getY() << std::endl;
					characters::playable[j] -> setXY(newX, newY);
					if(characters::playable[j]->getHealth() == 0) characters::playable[j]->setRoomId(newRoom);
					std::cout << "character " << j << " at " << pc->getX() << "," << pc->getY() << std::endl;
				}
				
				
				loadMap(doors[i].targetMap);
				
				return;
			}
		}
	}
	
	bool hasRoomFlag(int flags) {
		return (currentRoom->eventFlags & flags) == flags;
	}
	
	void setRoomFlags(int flags) {
		currentRoom->eventFlags |= flags;
	}
	
	bool hasGlobalFlag(int flag) {
		return globalFlags.find(flag) != globalFlags.end();
	}
	
	void addGlobalFlag(int flag) {
		globalFlags.insert(flag);
	}
	
	void removeGlobalFlag(int flag) {
		globalFlags.erase(flag);
	}
	
	int itemPileSpriteIndex(int x, int y){
		int index = 0;

		auto items = currentRoom->itemPiles.find(std::make_pair(x,y));
		if(items != currentRoom->itemPiles.end()){
			for(int i=0; i<items->second->size(); ++i){
				if((*items->second)[i]->isWeapon()) {
				
					switch(static_cast<weapon*>((*items->second)[i])->wclass){
						case 0: index |= 0x10; break;
						case 1: index |= 0x01; break;
						default: index |= 1;
					}
				} else {
					index |= (1 << items::itemCategoryToSpriteIndex(static_cast<item*>((*items->second)[i])->category));
				}
			}
		}
		
		return index;
	}
	
	StattedCharacter* getCharacterAt(int x, int y){
		for(int i=0; i<characters::playable.size(); ++i){
			if(characters::playable[i]->getX() == x && characters::playable[i]->getY() == y && characters::playable[i]->getRoomId() == currentId && characters::playable[i]->getHealth() > 0) {
				return characters::playable[i];
			}
		}
		return getEnemyAt(x,y);
	}
	
	Enemy* getEnemyAt(int x, int y){
		const std::vector<Enemy*>& roomEnemies = getEnemies();
		for(int i=0; i<roomEnemies.size(); ++i){
			if(roomEnemies[i]->getX() == x && roomEnemies[i]->getY() == y) {
				if(roomEnemies[i]->getHealth() <= 0) return NULL; // dead enemies don't count
				return roomEnemies[i];
			}
		}
		return NULL;
	}
	
	std::shared_ptr<NPC> getNPCAt(int x, int y) {
		for(int i=0; i<npcs.size(); ++i){
			if(npcs[i]->getX() == x && npcs[i]->getY() == y) {
				return npcs[i];
			}
		}
		return nullptr;
	}
	
	void startEnemyTurn() {
		
		// clear out dead enemies
		for(auto itr=currentRoom->enemies.begin(); itr!=currentRoom->enemies.end();){
			if((*itr)->getHealth() <= 0){
				Enemy* temp = *itr;
				itr = currentRoom->enemies.erase(itr);
				delete temp;
			} else ++itr;
		}
		
		if(currentRoom->enemies.size() > 0) {
			playerTurn = false;
			buttons::setInteractive(false);
		
			for(int i=0; i<currentRoom->enemies.size(); ++i){
				currentRoom->enemies[i]->beginTurn();
			}
			
		} else {
			enableCombat(false);
			buttons::setInteractive(true);
			buttons::showMainTurnMenu();
		}
	}
	
		
	bool playerTurnDone() {
		for(int i=0; i<characters::playable.size(); ++i){
			if(! characters::playable[i]->isDone() ) return false;
		}

		return true;
	}
	
	void handlePlayerDeath() {
		auto itr = allRooms.begin();
		while(itr != allRooms.end()){
			if(itr->second == currentRoom) {
				++itr;
			} else {
				deleteRoomCache(itr->first, itr->second);
				itr = allRooms.erase(itr);
			}
		}
		buttons::showDeathScreen();
	}
	
	
	void checkPlayerTurn() {
		if(playerTurn) return;
		for(int i=0; i<currentRoom->enemies.size(); ++i){
			if(!currentRoom->enemies[i]->isDone()) return;
		}
		playerTurn = true;
		bool dead = true;
		for(int i=0; i<characters::playable.size(); ++i) {
			if(characters::playable[i]->getHealth() > 0) dead = false;
		}
		if(dead) {
			handlePlayerDeath();
		} else {
			buttons::setInteractive(true);
			characters::startPlayerTurn();
		}
	}
	
	void onRespawn() {
		deleteRoomCache(currentId, currentRoom);
		allRooms.erase(currentId);
		loadSave();
	}
	
	bool outOfBounds(int x, int y) {
		return (x<0 || y<0 || x>=width || y>=height);
	}
	
	bool blocked(int x, int y){
		if(outOfBounds(x, y)) return true;
		return currentRoom->types[y][x] & FLAG_BLOCKED;
	}
	
	bool blockedOrOccupied(int x, int y){
		return blocked(x, y) || !!getCharacterAt(x,y);
	}
	
	bool isThrowTarget(int itemIndex, int x, int y) {
		if(outOfBounds(x, y)) return false;
    if(currentRoom->types[y][x] & FLAG_CUSTOM == FLAG_CUSTOM){
      auto itr = customTerrains.find(currentRoom->types[y][x]);
      if(itr != customTerrains.end()){
        for (customTerrainEffect effect : itr->second) {
          if(effect.type == 't' && 
          (effect.params[0]==itemIndex || effect.params[1]==-1)) return true;
        }
      }
    }
		return !( (currentRoom->types[y][x] & FLAG_CRATE) == FLAG_BLOCKED );
	}
	
	void onLook(int x, int y){
		for(int i=0; i<currentRoom->checkpoints.size(); ++i) {
			if(currentRoom->checkpoints[i].x == x && currentRoom->checkpoints[i].y == y) {
				mostRecentCheckpoint = i;
				cutscenes::loadDialogueFile(-1);
				cutscenes::startDialogue(currentRoom->checkpoints[i].charge);
				return;
			}
		}
		std::shared_ptr<NPC> npc = getNPCAt(x, y);
		if(npc) {
			npc->startDialogue();
			return;
		}
		Enemy* e = getEnemyAt(x,y);
		if(e && !e->hasFlags(ENEMY_INVIS)) {
			buttons::onEnemyLook(e);
		} else for(int i=0; i<lookTexts.size(); ++i) if(lookTexts[i].x==x && lookTexts[i].y==y) {
			cutscenes::startDialogue(lookTexts[i].index);
		}
	}
	
	void handleCheckpoint(int charge) {
		if(charge & 0x02) { // health
			for(int i=0; i<characters::playable.size(); ++i) {
				characters::playable[i]->heal(characters::playable[i]->getMaxHealth());
			}
		}
		if(charge & 0x04) { // spirit
			for(int i=0; i<characters::playable.size(); ++i) {
				characters::playable[i]->setSpirit(characters::playable[i]->getMaxSpirit());
				display::sparkleSpiritMeter(characters::playable[i]);
			}
		}
		if(charge & 0x01) { // save 
			save();
		}
		
		if(currentRoom->dungeonId) {
			currentRoom->checkpoints[mostRecentCheckpoint].charge &= ~0x06;
		}
	}
	
	void removeNpc(int npcId) {
		for(auto itr = npcs.begin(); itr != npcs.end(); ++itr) {
			if((*itr)->getId() == npcId) {
				npcs.erase(itr);
				return;
			}
		}
	}
	
	void npcWalk(int npcId, direction dir, int steps) {
		std::shared_ptr<NPC> npc;
		for(int i=0; i<npcs.size(); ++i) {
			if(npcs[i]->getId() == npcId) {
				npc = npcs[i];
			}
		}
		if(npc == NULL) return;
		
		npc->takeSteps(dir, steps);
	}
}
