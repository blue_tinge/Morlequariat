#ifndef __ANIMATIONS__
#define __ANIMATIONS__

#include <SFML/Graphics.hpp>
#include <functional>

#include "core.h"

#define PROJECTILE_FRAMES 6

namespace animations {
	struct animation {
		int frames;
		int speed;
		bool loop;
		sf::IntRect* frameRects;
		sf::Texture* texture;
	};
	
	void init();
	
	extern animation takeDamage[NUM_DAMAGE_TYPES]; // TODO add more damage types
	
	extern animation gainHealth;
	extern animation gainSpirit;
	
	extern animation airBuff; // TODO store all buff animations together
	
	class AnimationInstance {
		public:
			AnimationInstance(int, int, bool, animations::animation*);
			
			const int offsetX;
			const int offsetY;
			const bool atTile;
			
			sf::Sprite* update();
		
		private:
			const animations::animation* a;
			int start;
			int frame;
			sf::Sprite sprite;
	};
	
	class Projectile {
		private:
			int sourceX;
			int sourceY;
			int destX;
			int destY;
			int startTime;
			sf::Sprite sprite;
			
			
		public:
			sf::Sprite* getSprite() { return &sprite; }
			int getCurrentX() const;
			int getCurrentY() const;
			
			bool isDone() const;
			
			Projectile(int, int, int, int, sf::Texture&, int, int);
	};
	
	void animateProjectile(int, int, int, int, damage_type, std::function<void()>);
	void animateItemProjectile(int, int, int, int, int, std::function<void()>);
}

#endif
