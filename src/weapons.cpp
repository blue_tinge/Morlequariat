#include <iostream>
#include <fstream>
#include <sstream>

#include "weapons.h"
//#include "characters.h"

using namespace std;

class StattedCharacter;
class PlayerCharacter;

namespace weapons {
	vector<weapon*> allWeapons;
	weapon* getWeapon(int id){
		return allWeapons[id];
	}
	
	sf::Texture* weaponsTexture;
	
	weaponSpecialEffect readEffect(stringstream& ss) {
		weaponSpecialEffect output;
		char type;
		ss >> type;
		int params;
		switch(type) {
			//case 'A': // aoe
			case 'd': params = 3; output.type = weaponEffectType::debuff; break;
			case 'D': params = 3; output.type = weaponEffectType::stackingDebuff; break;
			case 'a': params = 1; output.type = weaponEffectType::adapt;  break;
			case 'p': params = 1; output.type = weaponEffectType::push;   break;
			case 'r': params = 1; output.type = weaponEffectType::refund; break;
			case 's': params = 2; output.type = weaponEffectType::splash; break;
			
		}
		output.params = new int[params];
		for(int i=0; i<params;++i) ss >> output.params[i];
		return output;
	}
	
	void init(){
		weaponsTexture = new sf::Texture;
		weaponsTexture->loadFromFile("img/all_weapons.png");
		
		int tilesetWidth = weaponsTexture->getSize().x / ITEM_IMGSIZE;
		//int tilesetHeight = weaponsTexture->getSize().y / ITEM_IMGSIZE;
		
		//Adhoc Serialized Data Format
		ifstream weaponFile("misc/weapons.asdf");
		string line;
		stringstream lineStream;
		int id = 0;
		while(getline(weaponFile, line)){
			lineStream.str(line);
			lineStream.clear();
			
			weapon* w = new weapon;
			w->id = id;
			getline(lineStream, w->name, '\t');
			getline(lineStream, w->flavor, '\t');
			string temp;
			getline(lineStream, temp, '\t');
			w->wclass = (weapon_class)stoi(temp);
			
			getline(lineStream, temp, '\t');
			w->rtype = (range_type)stoi(temp);
			
			getline(lineStream, temp, '\t');
			w->baseRange = stoi(temp);
			
			for(int i=0; i<MAX_COMMIT; ++i) {
				getline(weaponFile, line);
				lineStream.str(line);
				lineStream.clear();
				
				getline(lineStream, temp, '\t');
				w->effects[i].dtype = (damage_type)stoi(temp);
				
				getline(lineStream, temp, '\t');
				w->effects[i].baseDamage = stoi(temp);
				
				while(lineStream.rdbuf()->in_avail()){
					w->effects[i].extraEffects.push_back(readEffect(lineStream));
				}
			}
			
			w->sprite = new sf::Sprite;
			w->sprite->setTexture(*weaponsTexture);
			w->sprite->setTextureRect(sf::IntRect(id%tilesetWidth * ITEM_IMGSIZE, id/tilesetWidth * ITEM_IMGSIZE, ITEM_IMGSIZE, ITEM_IMGSIZE));
			
			allWeapons.push_back(w);
			++id;
		}
	}
	
	std::vector<std::pair<int, int> > getTargets(range_type rtype, int range){
		std::vector<std::pair<int, int> > targets;
		switch(rtype){
			case melee:
				targets.push_back(std::make_pair(1, 0));
				targets.push_back(std::make_pair(0, 1));
				targets.push_back(std::make_pair(-1, 0));
				targets.push_back(std::make_pair(0, -1));
				break;
			case radius:
			case long_melee:
				std::cout << "Weapon is radius" << std::endl;
				for(int i = -range; i <= range; ++i){
					for(int j = abs(i)-range; j <= range-abs(i); ++j){
						if(i || j) targets.push_back(std::make_pair(i,j));
					}
				}
				break;
			case linear:
				std::cout << "Weapon is linear" << std::endl;
				for(int i=1; i<=range; ++i){
					targets.push_back(std::make_pair(i, 0));
					targets.push_back(std::make_pair(0, i));
					targets.push_back(std::make_pair(-i, 0));
					targets.push_back(std::make_pair(0, -i));
				}
		}
		return targets;
	}

	std::vector<std::pair<int, int> > getTargets(weapon* weapon, int range){
		if(weapon->rtype == range_type::long_melee) range = 0;
		return getTargets(weapon->rtype, weapon->baseRange + range);
	}
}
