#ifndef __WORLD__
#define __WORLD__

#include <map>
#include <vector>
#include <memory>

#include "core.h"
#include "items.h"

#define FLAG_BLOCKED 0x01
#define FLAG_NOSTAND 0x02
#define FLAG_WATER   0x04
#define FLAG_LAVA    0x08
#define FLAG_BREAK   0x10
#define FLAG_ONEWAY  0x20
#define FLAG_CUSTOM  0x40
#define FLAG_DOOR    0x80
#define LIQUID_MASK  (FLAG_WATER | FLAG_LAVA)
#define FLAG_CRATE   (FLAG_BLOCKED | FLAG_BREAK)

#define CRATE_TILE 0xfe

class StattedCharacter;
class PlayerCharacter;
class Enemy;
class NPC;

namespace world {
	void loadMap(int mapid);
	void setTerrain(int x, int y, int t);
	int getTerrain(int x, int y);
	void setTileImage(int x, int y, int t);
	int getTileImage(int x, int y);
	
	unsigned char** getAllTileImages();
	
	int getRoomId();
	
	void loadSave();
	
	//extern std::map<std::pair<int,int>,std::vector<ItemOrWeapon*>*> itemPiles;
	const std::vector<Enemy*>& getEnemies();
	const std::vector<std::shared_ptr<NPC>>& getNPCs();
	
	Enemy* getEnemyAt(int,int);
	StattedCharacter* getCharacterAt(int,int);
	
	int countItemsAt(int,int);
	const std::vector<ItemOrWeapon*>& getItemsAt(int,int);
	void removeNthItemAt(int,int,int);
	void putItem(int, int, ItemOrWeapon*);
	
	void damageAtTile(int, int, int, damage_type);
	
	void processDoor(PlayerCharacter*, int, int);
	
	int itemPileSpriteIndex(int x, int y);
	
	bool outOfBounds(int, int);
	bool blocked(int, int);
	bool blockedOrOccupied(int, int);
	
	bool isThrowTarget(int itemIndex, int x, int y);
	
	void handleCustomTerrain(int, StattedCharacter*);
	
	void handleCheckpoint(int);
	
	extern unsigned char** types;
	
	extern int width;
	extern int height;
	
	void addNpc(int, int, int, int);
	void removeNpc(int);
	void npcWalk(int, direction, int);
	
	void onLook(int, int);
	
	bool playerTurnDone();
	void startEnemyTurn();
	void checkPlayerTurn();
	
	void onRespawn();
	
	bool hasRoomFlag(int);
	void setRoomFlags(int);
	
	bool hasGlobalFlag(int);
	void setGlobalFlag(int);
	void removeGlobalFlag(int);
	
	void enableCombat(bool);
	bool isCombat();
}

#endif
