#ifndef __DISPLAY__
#define __DISPLAY__

#include <iostream>
#include <vector>
#include <random>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "core.h"
#include "characters.h"
#include "display.h"
#include "buttons.h"
#include "world.h"
#include "animations.h"
#include "timekeeping.h"
#include "music.h"
//#include "status.h"

using namespace std;

#define VIEW_INVENTORY 1
#define VIEW_SPELL_LIST 2
#define VIEW_GROUND_ITEMS 3
#define VIEW_CHAR_STATUS 4
#define VIEW_DEATH_SCREEN 5
#define VIEW_SPELL_DESCRIPTION 6

#define CHARSPEED 1
#define CHARWIDTH 12
#define CHARHEIGHT 20
//#define TEXTWIDTH 50

#define DMGCHARW 9
#define DMGCHARH 14

#include <sstream>

const int TILESIZE = 48;

namespace cutscenes {
	void setAdvanceable();
}

namespace display {
	
	int globalScale = 1;

	namespace {
		sf::Sprite tileStamp;
		sf::RenderTexture bg_texture;
		sf::Sprite background;
		
		sf::RenderTexture windowTexture;
		sf::Sprite windowSprite;

		sf::Texture* tilesetTexture;
		int tilesetWidth;
		int tilesetHeight;

		sf::Texture oobTexture;
		sf::Sprite outOfBounds;

		// these are in tiles, not pixels
		int offsetX = 0;
		int offsetY = 0;
		
		int _view;
		
		int mouseX, mouseY;
		
		sf::Texture meters;
		sf::Sprite health1;
		sf::Sprite health2;
		sf::Sprite spirit1;
		sf::Sprite spirit2;		
		sf::Sprite speed1;
		sf::Sprite speed2;
		sf::Sprite iconHealth;
		sf::Sprite iconSpirit;
		sf::Sprite iconSpeed;
		sf::Sprite iconAction;
		
		vector<animations::AnimationInstance*> animations;
		vector<shared_ptr<animations::Projectile>> projectiles;
		
		sf::Texture itemPileTexture;
		sf::Sprite itemPileSprite;
		
		sf::Sprite charSprite;
		/*sf::Texture fontTexture;
		sf::Texture fontTextureRed;
		sf::Texture fontTextureBlue;*/
		sf::Texture fontTextures[4];
		//std::list<std::string> dialogueTextList;
				
		sf::Texture dmgFontTexture;
		
		sf::Sprite buffIconSprite;
		sf::Texture buffIconTexture;
		
		bool isDialogue = false;
		int dialogueStartTime;
		int dialogueTickCount;
		string dialogueText;
		string dialogueName;
		int dialogueSoundId;
		int dialogueSpriteId;
		int dialogueResponseCount;
		shared_ptr<std::vector<std::string> > dialogueResponseOptions;

		bool isSceneImage;
		sf::Texture sceneImageTexture;
		sf::Sprite sceneImageSprite;
		
		sf::Texture textboxTexture;
		sf::Sprite dialogueTextboxSprite;
		
		sf::Texture statusPageTexture;
		sf::Sprite statusTextboxSprite;
		
		sf::Texture talkspriteTexture;
		sf::Sprite dialogueFaceSprite;
		
		sf::Texture itemDescriptionBoxTexture;
		sf::Sprite itemDescriptionBox;
		
		sf::Texture selectItemTexture;
		sf::Sprite selectItemSprite;
		
		Enemy* viewedEnemy;
	}
	
	//sf::Font font;
	void init(){
		//font.loadFromFile("misc/axolotl.ttf");
		tilesetTexture = new sf::Texture;
		
		windowTexture.create(SCREENWIDTH+128,SCREENHEIGHT+64);
		
		meters.loadFromFile("img/meters.png");
		health1.setTexture(meters);
		health1.setTextureRect(sf::IntRect(0,0,99,7));
		health2.setTexture(meters);
		health2.setTextureRect(sf::IntRect(100,0,103,7));
		spirit1.setTexture(meters);
		spirit1.setTextureRect(sf::IntRect(0,7,99,7));
		spirit2.setTexture(meters);
		spirit2.setTextureRect(sf::IntRect(100,7,103,7));
		speed1.setTexture(meters);
		speed1.setTextureRect(sf::IntRect(0,14,7,7));
		speed2.setTexture(meters);
		speed2.setTextureRect(sf::IntRect(7,14,7,7));
		
		iconHealth.setTexture(meters);
		iconHealth.setTextureRect(sf::IntRect(149,14,13,13));
		iconSpirit.setTexture(meters);
		iconSpirit.setTextureRect(sf::IntRect(162,14,15,15));
		iconSpeed.setTexture(meters);
		iconSpeed.setTextureRect(sf::IntRect(177,14,13,11));
		iconAction.setTexture(meters);
		iconAction.setTextureRect(sf::IntRect(190,14,11,17));
		
		itemPileTexture.loadFromFile("img/item_overworld.png");
		itemPileSprite.setTexture(itemPileTexture);
		
		for(int i=0; i<4; ++i) fontTextures[i].loadFromFile("img/font/"+to_string(i)+".png");
		
		charSprite.setTexture(fontTextures[0]);
		
		dmgFontTexture.loadFromFile("img/font/dmg.png");
		
		buffIconTexture.loadFromFile("img/buffs.png");
		buffIconSprite.setTexture(buffIconTexture);
		
		textboxTexture.loadFromFile("img/textbox.png");
		dialogueTextboxSprite.setTexture(textboxTexture);
		dialogueTextboxSprite.setPosition(48,SCREENHEIGHT-191);
				
		statusPageTexture.loadFromFile("img/statuspage.png");
		statusTextboxSprite.setTexture(statusPageTexture);
		statusTextboxSprite.setPosition(48,48);
		
		talkspriteTexture.loadFromFile("img/talksprites.png");
		dialogueFaceSprite.setTexture(talkspriteTexture);
		dialogueFaceSprite.setPosition(55,SCREENHEIGHT - 184);
		
		sceneImageTexture.loadFromFile("img/scene/0.png");
		sceneImageSprite.setTexture(sceneImageTexture);
		sceneImageSprite.setPosition(217, SCREENHEIGHT - 358);
		//sceneImageSprite.setTextureRect(sf::IntRect(217, 242, 320, 128));
		
		itemDescriptionBoxTexture.loadFromFile("img/invbox.png");
		itemDescriptionBox.setTexture(itemDescriptionBoxTexture);
		itemDescriptionBox.setPosition(ITEM_IMGSIZE * 4, ITEM_IMGSIZE/2);
		
		selectItemTexture.loadFromFile("img/select.png");
		selectItemSprite.setTexture(selectItemTexture);
		selectItemSprite.setTextureRect(sf::IntRect(TILESIZE+ITEM_IMGSIZE, ITEM_IMGSIZE*3, ITEM_IMGSIZE, ITEM_IMGSIZE));
		
	}
	
	void setGlobalScale(int scale) {
		globalScale = scale;
		windowSprite.setScale(scale,scale);
	}
	
	int getGlobalScale() {
		return globalScale;
	}
	
	bool isClearView(){return _view==0;}
	void clearView(){_view = 0;}
	void showInventory(){_view = VIEW_INVENTORY;}
	void showSpellList(){_view = VIEW_SPELL_LIST;}
	void showSpellDescription(){_view = VIEW_SPELL_DESCRIPTION;}
	void showGroundItems(){_view = VIEW_GROUND_ITEMS;}
	void showCharacterStatus(){
		viewedEnemy = NULL;
		_view = VIEW_CHAR_STATUS;
	}
	void showDeathScreen(){
		_view = VIEW_DEATH_SCREEN;
		dialogueStartTime = timekeeping::currentTick();
	}

	bool centerOnCharacter = false;
	Character* centerCharacter = NULL;

	void centerCoords(int x, int y) {
		offsetX = x - (SCREENWIDTH/2/TILESIZE);
		offsetY = y - (SCREENHEIGHT/2/TILESIZE);
	}
	
	void center(int x, int y){
		centerOnCharacter = false;
		centerCoords(x, y);
	}

	void center(Character* character){
		centerOnCharacter = true;
		centerCharacter = character;
	}
	
	std::list<std::pair<std::pair<int, int>, int>> tileUpdateQueue;
	
	void scheduleSetTile(int x, int y, int t){
		tileUpdateQueue.push_back(std::make_pair(std::make_pair(x,y),t));
	}
	
	void stampTile(int x, int y, int t) {
		tileStamp.setTextureRect(sf::IntRect((t%tilesetWidth)*TILESIZE, (t/tilesetWidth)*TILESIZE, TILESIZE, TILESIZE ));
		tileStamp.setPosition(x*TILESIZE, y*TILESIZE);
		bg_texture.draw(tileStamp);
	}
	
	void setPartialTile(int x, int y, int xQuarters, int yQuarters, int t, int tXQ, int tYQ){
		tileStamp.setTextureRect(sf::IntRect((t%tilesetWidth)*TILESIZE + (tXQ * TILESIZE / 4), (t/tilesetWidth)*TILESIZE + (tYQ * TILESIZE / 4), TILESIZE/4, TILESIZE/4 ));
		tileStamp.setPosition(x*TILESIZE + (xQuarters * TILESIZE / 4), y*TILESIZE + (yQuarters * TILESIZE / 4));
		bg_texture.draw(tileStamp);
	}
	
	void setInterpolatedTile(int x, int y, int t, unsigned char** imgIds, int width, int height) {
		if (y > 0 && imgIds[y-1][x] != t) {
			setPartialTile(x, y, 1, 0, t, 1, 0);
			setPartialTile(x, y, 2, 0, t, 2, 0);
		
			if (x > 0 && imgIds[y][x-1] != t) {
				setPartialTile(x, y, 0, 0, t, 0, 0);
				setPartialTile(x, y, 0, 1, t, 0, 1);
				setPartialTile(x, y, 0, 2, t, 0, 2);
			} else {
				setPartialTile(x, y, 0, 0, t, 1, 0);
			}
			
			if (x < width-1 && imgIds[y][x+1] != t) {
				setPartialTile(x, y, 3, 0, t, 3, 0);
				setPartialTile(x, y, 3, 1, t, 3, 1);
				setPartialTile(x, y, 3, 2, t, 3, 2);
			} else {
				setPartialTile(x, y, 3, 0, t, 2, 0);
			}
			
		} else {
			if (x > 0 && imgIds[y][x-1] != t) {
				setPartialTile(x, y, 0, 0, t, 0, 1);
				setPartialTile(x, y, 0, 1, t, 0, 1);
				setPartialTile(x, y, 0, 2, t, 0, 2);
			} else if(x > 0 && y > 0 && imgIds[y-1][x-1] != t) {
				setPartialTile(x, y, 0, 0, t, 1, 1);
			}
			if (x < width-1 && imgIds[y][x+1] != t) {
				setPartialTile(x, y, 3, 0, t, 3, 1);
				setPartialTile(x, y, 3, 1, t, 3, 1);
				setPartialTile(x, y, 3, 2, t, 3, 2);
			} else if(x < width-1 && y > 0 && imgIds[y-1][x+1] != t) {
				setPartialTile(x, y, 3, 0, t, 2, 1);
			}
		}
		
		if (y < height-1 && imgIds[y+1][x] != t) {
			setPartialTile(x, y, 1, 3, t, 1, 3);
			setPartialTile(x, y, 2, 3, t, 2, 3);
			
			if (x > 0 && imgIds[y][x-1] != t) {
				setPartialTile(x, y, 0, 3, t, 0, 3);
			} else {
				setPartialTile(x, y, 0, 3, t, 1, 3);
			}
			if (x < width-1 && imgIds[y][x+1] != t) {
				setPartialTile(x, y, 3, 3, t, 3, 3);
			} else {
				setPartialTile(x, y, 3, 3, t, 2, 3);
			}
		} else {
			if (x > 0 && imgIds[y][x-1] != t) {
				setPartialTile(x, y, 0, 3, t, 0, 2);
			} else if(x > 0 && y < height-1 && imgIds[y+1][x-1] != t) {
				setPartialTile(x, y, 0, 3, t, 1, 2);
			}
			if (x < width-1 && imgIds[y][x+1] != t) {
				setPartialTile(x, y, 3, 3, t, 3, 2);
			} else if(x < width-1 && y < height-1 && imgIds[y+1][x+1] != t) {
				setPartialTile(x, y, 3, 3, t, 2, 2);
			}
		}
	}
	
	void setTile(int x, int y, int t, unsigned char** imgIds, int width, int height){
		if( (t & 0xF1) == 0xF1 ) {
			stampTile(x, y, t - 1);
			setInterpolatedTile(x, y, t, imgIds, width, height);
		} else {
			stampTile(x, y, t);
		}
	}
	

	void loadBackground(unsigned char** imgIds, unsigned char** tileIds, int width, int height, int tileset){
		bg_texture.create(width*TILESIZE, height*TILESIZE);

		tilesetTexture->loadFromFile("img/tiles/"+std::to_string(tileset)+".png");
		tilesetWidth = tilesetTexture->getSize().x / TILESIZE;
		tilesetHeight = tilesetTexture->getSize().y / TILESIZE;

		tileStamp.setTexture(*tilesetTexture);
		
		for(int i=0; i<height; ++i){
			for(int j=0; j<width; ++j){
				
				setTile(j, i, imgIds[i][j], imgIds, width, height);
				
				if((tileIds[i][j] & FLAG_CRATE) == FLAG_CRATE) {
					stampTile(j, i, CRATE_TILE);
				}
			}
		}

		bg_texture.display();
		background = sf::Sprite(bg_texture.getTexture());
		
		oobTexture.loadFromFile("img/bg/"+std::to_string(tileset)+".png");
		oobTexture.setRepeated(true);
		outOfBounds.setTexture(oobTexture);
	}

	void updateMouse(int x, int y){
		mouseX = x / globalScale;
		mouseY = y / globalScale;
	}
	
	void addAnimation(int x, int y, animations::animation* a){
		animations.push_back(new animations::AnimationInstance(x, y, false, a));
	}
	
	void addAnimationAtTile(int x, int y, animations::animation* a){
		animations.push_back(new animations::AnimationInstance(x, y, true, a));
	}
	
	int getEscapedWidth(std::string text){
		int width = 0;
		int line = 0;
		for(int i=0; i<text.length(); ++i) {
			if(text[i]=='\\') line-=2;
			else if(text[i] == '\n') {
				if(line > width) width = line;
				line = 0;
			}
			else ++line;
		}
		if(line > width) width = line;
		return width;
	}
	
	int getEscapedLength(std::string text){
		int line = 0;
		for(int i=0; i<text.length(); ++i) {
			if(text[i]=='\\') line-=2;
			else ++line;
		}
		return line;
	}
	
	std::string padInt(int i, int width){
		std::string output = to_string(i);
		while(output.length() < width) output = " " + output;
		return output;
	}

	void skipDialogue() {
		dialogueStartTime = 0;
	}
	
	void showDialogue(const std::string& text, const std::string& name, int talkspriteid, int voiceid) {
		isDialogue = true;
		dialogueText = text;
		dialogueName = name;
		dialogueSpriteId = talkspriteid;
		dialogueSoundId = voiceid;
		dialogueStartTime = timekeeping::currentTick();
		dialogueTickCount = 0;
		dialogueResponseCount = 0;
	}
	
	void showDialogueOptions(int count, const std::shared_ptr<std::vector<string> >& options) {
		dialogueResponseCount = count;
		dialogueResponseOptions = options;
	}
	
	void hideDialogue(){
		isDialogue = false;
		isSceneImage = false;
	}
	
	void showSceneImage(int index) {
		if(index < 0) isSceneImage = false;
		else {
			isSceneImage = true;
			sceneImageTexture.loadFromFile("img/scene/"+std::to_string(index)+".png");
			sceneImageSprite.setTexture(sceneImageTexture);
		}
	}
	
	/*void startDialogue(const std::string& text){
		dialogueTextList.clear();
		dialogueTextList.push_back(text);
		dialogueStart = timekeeping::currentTick();
	}
	
	void startDialogue(std::list<std::string>& text){
		dialogueTextList = text;
		dialogueStart = timekeeping::currentTick();
	}
	
	void advanceDialogue(){
		if(dialogueTextList.size() && getEscapedWidth(*dialogueTextList.begin()) <= (timekeeping::currentTick() - dialogueStart)/CHARSPEED) {
			dialogueTextList.pop_front();
			dialogueStart = timekeeping::currentTick();
		}
	}*/

	void showEnemyStatus(Enemy* e){
		viewedEnemy = e;
		_view = VIEW_CHAR_STATUS;
	}

	struct damageText {
		int amt;
		int x;
		int y;
		int endTick;
	};
	
	std::vector<damageText> damageTexts;
	
	void showDamageText(int amt, int x, int y){
		damageText dt;
		dt.amt = amt;
		dt.x = x;
		dt.y = y;
		dt.endTick = timekeeping::currentTick() + 15;
		damageTexts.push_back(dt);
	}

	void sparkleHealthMeter(PlayerCharacter* pc){
		addAnimation(SCREENWIDTH+21, 128*pc->getId()+76, &animations::gainHealth);
	}
	
	void sparkleSpiritMeter(PlayerCharacter* pc){
		addAnimation(SCREENWIDTH+21, 128*pc->getId()+92, &animations::gainSpirit);
	}
	
	void addProjectile(shared_ptr<animations::Projectile> proj) {
		projectiles.push_back(proj);
	}
	
	void spliceStatText(std::string& text, int startIndex) {
		int removeLength;
		std::string newText;
		
		PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
		
		switch(text[startIndex]) {
			case 'r': { // range
				newText = "\\R" + std::to_string(pc->getRange() + stoi(text.substr(startIndex+1, 2))) + "\\c";
				removeLength = 3;
				break;
			}
			case 'a': { // outgoing attack, typed
				damage_type dt = (damage_type) (text[startIndex+1] - '0');
				newText = "\\R" + std::to_string(stoi(text.substr(startIndex+2, 3)) * pc->getDamageDealt(dt) / 100) + "\\S" + text[startIndex+1] + "\\c";
				removeLength = 5;
				break;
			}
			case 'd': { // incoming attack, typed
				damage_type dt = (damage_type) (text[startIndex+1] - '0');
				newText = "\\R" + std::to_string(stoi(text.substr(startIndex+2, 3)) * pc->getDamageTaken(dt) / 100) + "\\S" + text[startIndex+1] + "\\c";
				removeLength = 5;
				break;
			}
			case 'w': { // weapon-based attack
				weapon* w = pc->getEquippedWeapon();
				newText = "\\R" + std::to_string(w->effects[0].baseDamage * pc->getDamageDealt(w->effects[0].dtype) / 100) + "\\S" + (char)(w->effects[0].dtype + '0') + "\\c";
				removeLength = 1;
				break;
			}
				
		}
		
		text.erase(startIndex, removeLength);
		text.insert(startIndex, newText);
	}

	void drawText(int originX, int originY, std::string text, int maxWidth = 50){
		//(timekeeping::currentTick() - dialogueStart)/CHARSPEED
		// Draw background
		
		int tileset = 0;
		
		int readPos=0;
		int writePos=0;
		while(readPos < text.length()) {
			if(text[readPos] == '\\'){
				++readPos;
				switch(text[readPos]){
					case 'R': tileset=1; break; // red
					case 'B': tileset=2; break; // blue
					case 'S': tileset=3; break; // special characters
					case 'c': tileset=0; break; // clear color
					case 'n': originY += CHARHEIGHT; writePos = 0; break; // newline
					case '\\': { // character stat escape sequences
						spliceStatText(text, readPos+1);
					}
				}
			} else if(text[readPos] == '\n'){
				originY += CHARHEIGHT;
				writePos = 0;
			} else {

				charSprite.setTexture(fontTextures[tileset]);


				charSprite.setTextureRect(sf::IntRect((text[readPos]-' ') % 12 * CHARWIDTH, (text[readPos]-' ') / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + writePos % maxWidth * CHARWIDTH;
				int y = originY + writePos / maxWidth * CHARHEIGHT;				
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writePos;
			}
			++readPos;
		}
	}
	
	void narrateText(int originX, int originY, std::string text, int voiceId, int maxWidth = 50){
				
		//(timekeeping::currentTick() - dialogueStart)/CHARSPEED
		// Draw background
		
		int maxLength = (timekeeping::currentTick() - dialogueStartTime)/CHARSPEED;
		
		int tileset = 0;
		bool wave = false;
		int vibrate = 0;
		
		int delay = 1;
		bool voicePunctuation = false;
		
		int readPos=0;
		int writePos=0;
		int writeTime=0;
		while(writeTime < maxLength && readPos < text.length()) {
			if(text[readPos] == '\\'){
				++readPos;
				switch(text[readPos]){
					case 'R': tileset=1; break; // red
					case 'B': tileset=2; break; // blue
					case 'S': tileset=3; break; // special characters
					case 'c': tileset=0; break; // clear color
					case 'W': wave = true; break;
					case 'V': vibrate += 1; break; // vibrate
					case 't': wave=false; vibrate=0; break; // clear texture
					case 'P': delay *= 2; break; // slow
					case 'f': delay = 1; break; // normal speed
					case 'n': originY += CHARHEIGHT; writePos = 0; break; // newline
					case 'L': voicePunctuation = true; break; // Loud
					case 'q': voicePunctuation = false; break;
				}
			} else if(text[readPos] == '\n'){
				originY += CHARHEIGHT;
				writePos = 0;
			} else {

				charSprite.setTexture(fontTextures[tileset]);


				charSprite.setTextureRect(sf::IntRect((text[readPos]-' ') % 12 * CHARWIDTH, (text[readPos]-' ') / 12 * CHARHEIGHT, CHARWIDTH, CHARHEIGHT)); // 12 x 8 ASCII
				
				int x = originX + writePos % maxWidth * CHARWIDTH;
				int y = originY + writePos / maxWidth * CHARHEIGHT;
				if(vibrate) {
					x += rand() % (2*vibrate+1) - (vibrate+1);
					y += rand() % (2*vibrate+1) - (vibrate+1);
				} else if (wave) {
					int wavePos = (timekeeping::currentTick()+3*writePos) % 21 - 5;
					if(wavePos > 5) wavePos = 10 - wavePos;
					y += wavePos;
				}
				charSprite.setPosition(x,y);
				
				windowTexture.draw(charSprite);
				++writePos;
				writeTime += delay;
			}
			++readPos;
		}
		
		if(writeTime == maxLength && maxLength > dialogueTickCount) {
			if((voicePunctuation && text[readPos-1] != ' ') || (text[readPos-1] >= 'A' && text[readPos-1] <= 'z')) {
				double pitch = 1;
				
				if(wave) {
					pitch += .01 * (writeTime % 21);
				} else if(vibrate) {
					pitch += (rand() % 64) / 128.0 - (32/128.0);
				} else if(voiceId >= -1) {
					pitch += (rand() % 16) / 128.0 - (8/128.0);
				}
				
				if(tileset == 1 || tileset == 2) {
					pitch += .1;
				}
				
				
				if(voiceId < -1) voiceId = -1;
				
				music::playSound("voice/"+std::to_string(voiceId), pitch);
			}
			dialogueTickCount = maxLength;
			
		} else if(readPos == text.length() && _view != VIEW_DEATH_SCREEN) {
			cutscenes::setAdvanceable();
		}
	}
	
	void drawItemInfoPanel(ItemOrWeapon* i){
		windowTexture.draw(itemDescriptionBox);
		if(!i) return;
		drawText(4*ITEM_IMGSIZE + 7, ITEM_IMGSIZE/2 + 6, "\\B"+i->getName());
		drawText(4*ITEM_IMGSIZE + 7, ITEM_IMGSIZE/2 + 33, i->getFlavor(), (ITEM_IMGSIZE*6 - 14)/CHARWIDTH);
	}
	
	void drawSpellInfoPanel(std::shared_ptr<spell> s){
		//i->getSprite()->setPosition(8*ITEM_IMGSIZE, SCREENHEIGHT);
		//windowTexture.draw(*(i->getSprite()));
		//drawText(8*ITEM_IMGSIZE, SCREENHEIGHT, "\\B"+i->getName());
		drawText(8*ITEM_IMGSIZE, SCREENHEIGHT, s->description, (SCREENWIDTH+128 - 8*ITEM_IMGSIZE)/CHARWIDTH);
	}
	
	bool hasDialogue() {
		return isDialogue;
	}
	
	void drawWindow(sf::RenderWindow& window){
		// Handle queued tile updates
		while(!tileUpdateQueue.empty()){
			pair<pair<int,int>,int> update = tileUpdateQueue.front();
			tileUpdateQueue.pop_front();
			setTile(update.first.first, update.first.second, update.second, world::getAllTileImages(), world::width, world::height);
		}
		bg_texture.display();
		
		windowTexture.clear(sf::Color(103,103,103));
		
		outOfBounds.setTextureRect(sf::IntRect(offsetX*TILESIZE/2,offsetY*TILESIZE/2,SCREENWIDTH, SCREENHEIGHT));
		windowTexture.draw(outOfBounds);
		
		if(centerOnCharacter) centerCoords(centerCharacter->getX(), centerCharacter->getY());
		
		string tooltipText = "";
		
		// Health and spirit
		for(int i=0; i<characters::playable.size(); ++i){
			
			health1.setPosition(SCREENWIDTH+22, 128*i+76);
			windowTexture.draw(health1);
			spirit1.setPosition(SCREENWIDTH+22, 128*i+92);
			windowTexture.draw(spirit1);
			PlayerCharacter* pc = characters::playable[i];
			health2.setPosition(SCREENWIDTH+21+(100*pc->getHealth()/pc->getMaxHealth()), 128*i+76);
			windowTexture.draw(health2);
			
			spirit2.setPosition(SCREENWIDTH+21+(100*pc->getSpirit()/pc->getMaxSpirit()), 128*i+92);
			windowTexture.draw(spirit2);			
		}
		
		background.setPosition(-offsetX*TILESIZE, -offsetY*TILESIZE);
		background.setTextureRect(sf::IntRect(0,0, std::min((int)bg_texture.getSize().x, SCREENWIDTH+offsetX*TILESIZE), std::min((int)bg_texture.getSize().y, SCREENHEIGHT+offsetY*TILESIZE)));
		windowTexture.draw(background); // need to cast to int because they were uint
		
		for(int i=0; i < SCREENWIDTH/TILESIZE; ++i){
			for(int j=0; j < SCREENHEIGHT/TILESIZE; ++j){
				int x = i + offsetX;
				int y = j + offsetY;
				int p = world::itemPileSpriteIndex(x,y);
				if(p>0){
					itemPileSprite.setPosition(i*TILESIZE, j*TILESIZE);
					for(int q=0;1<<q<=p;++q) if (p&(1<<q)) {
						
						itemPileSprite.setTextureRect(sf::IntRect(
								q%(itemPileTexture.getSize().x/TILESIZE) * TILESIZE,
								q/(itemPileTexture.getSize().x/TILESIZE) * TILESIZE,
								TILESIZE,TILESIZE));
						windowTexture.draw(itemPileSprite);
					}
					
					if(itemPileSprite.getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
						if(world::countItemsAt(x,y) > 6){
							tooltipText = to_string(world::countItemsAt(x,y)) + " items...\n";
						} else {
							for(int i=0; i<world::countItemsAt(x,y); ++i){
								tooltipText += world::getItemsAt(x,y).at(i)->getName() + '\n';
							}
						}
					}
				}
			}
		}
		
		vector<Character*> charsByPosition;
		
		for(int i=0; i<characters::playable.size(); ++i){
			PlayerCharacter* c = characters::playable[i];
			if(c->getRoomId() != world::getRoomId()) continue; // they're in the next room, don't draw them
			c->updateAnimation();
			c->getSprite()->setPosition((c->getX()-offsetX)*TILESIZE, (c->getY()-offsetY)*TILESIZE -(int)c->getSprite()->getLocalBounds().height + TILESIZE);
			if(c->getSprite()->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
				tooltipText += c->getName()+'\n';
			}
			auto itr = charsByPosition.begin();
			while(itr != charsByPosition.end() && (*itr)->getY() < c->getY()) ++itr;
			charsByPosition.insert(itr, c);

		}
		
		const std::vector<std::shared_ptr<NPC>>& npcs = world::getNPCs();
		for(int i=0; i<npcs.size(); ++i){
			std::shared_ptr<NPC> c = npcs[i];
			c->updateAnimation();
			c->getSprite()->setPosition((c->getX()-offsetX)*TILESIZE + c->getOffsetX(), (c->getY()-offsetY)*TILESIZE -(int)c->getSprite()->getLocalBounds().height + TILESIZE + c->getOffsetY());
			windowTexture.draw(*c->getSprite());
			//if(c->getSprite()->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
			//	tooltipText += c->getName() + '\n';
			//}
			auto itr = charsByPosition.begin();
			while(itr != charsByPosition.end() && (*itr)->getY() < c->getY()) ++itr;
			charsByPosition.insert(itr, c.get());
		}
		
		for(int i=0; i<charsByPosition.size(); ++i) windowTexture.draw(*(charsByPosition[i]->getSprite()));
		
		const std::vector<Enemy*>& enemies = world::getEnemies();
		for(int i=0; i<enemies.size(); ++i){
			Enemy* c = enemies[i];
			if(c->getHealth() <= 0) continue; // don't draw dead enemies
			if(c->hasFlags(ENEMY_INVIS)) continue; // don't draw invisible enemies either
			c->updateAnimation();
			c->getSprite()->setPosition((c->getX()-offsetX)*TILESIZE, (c->getY()-offsetY)*TILESIZE);
			windowTexture.draw(*c->getSprite());
			if(c->getSprite()->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
				tooltipText += c->getName() + ", HP: " + std::to_string(c->getHealth()) + "/" + std::to_string(c->getMaxHealth()) + '\n';
			}
			const std::vector<statusCondition>& conditions = c->getStatusEffects();
			int buffStart = (c->getX() - offsetX) * TILESIZE + (TILESIZE - conditions.size()*17)/2;
			for(int j=0; j<conditions.size(); ++j){
				buffIconSprite.setTextureRect(sf::IntRect(16*(conditions[j].id%16), 16*(conditions[j].id/16),16,16));
				buffIconSprite.setPosition(buffStart + 17*j, (c->getY() - offsetY)*TILESIZE - 16);
				windowTexture.draw(buffIconSprite);
			}
		}
		
		if(_view == VIEW_DEATH_SCREEN) {
			narrateText((SCREENWIDTH - 40*12) / 2,(SCREENHEIGHT - 20) / 2, "But this is not how the story ends . . .", -1, 40);
			
		} else if(buttons::getSelectedCharacter() >= 0) {

			PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
			
			
			if(_view == VIEW_INVENTORY) {
				
				drawText(ITEM_IMGSIZE/2, ITEM_IMGSIZE/2 - CHARHEIGHT, "\\BEquipment");
				for(int i=0; i<EQUIPSPACE; ++i){
					if(pc->getEquipment()[i] > -1){
						sf::Sprite* s = items::getItem(pc->getEquipment()[i])->getSprite();
						s->setPosition(i*ITEM_IMGSIZE + ITEM_IMGSIZE/2, ITEM_IMGSIZE/2);
						windowTexture.draw(*s);
						if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
							tooltipText = items::getItem(pc->getEquipment()[i])->getName();
						}
					}
				}
				
				drawText(ITEM_IMGSIZE/2, ITEM_IMGSIZE*2 - CHARHEIGHT, "\\BInventory");
				for(int i=0; i<pc->getInventory().size();++i){
					item* it = pc->getInventory()[i];
					
					if(it){
						sf::Sprite* s = it->getSprite();
						s->setPosition((i%3)*ITEM_IMGSIZE + ITEM_IMGSIZE/2, (i/3 + 2)*ITEM_IMGSIZE);
						windowTexture.draw(*s);
						if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
							tooltipText = it->getName();
						}
					}
				}
				
				drawText(ITEM_IMGSIZE/2, ITEM_IMGSIZE*13/2 - CHARHEIGHT, "\\BWeapons");
				sf::Sprite* s = pc->getEquippedWeapon()->getSprite();
				s->setPosition(ITEM_IMGSIZE/2 + ITEM_IMGSIZE * 2, ITEM_IMGSIZE*13/2);
				windowTexture.draw(*s);
				if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
					tooltipText = pc->getEquippedWeapon()->getName();
				}
				
				const std::vector<weapon*> weapons = pc->getOffhandWeapons();
				for(int i=0; i<weapons.size();++i){
					if(weapons[i]){
						s = weapons[i]->getSprite();
						s->setPosition(ITEM_IMGSIZE/2 + i*ITEM_IMGSIZE, ITEM_IMGSIZE*13/2);
						windowTexture.draw(*s);
						if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
							tooltipText = weapons[i]->getName();
						}
					}
				}
				
				// Draw item info panel
				if(buttons::getSelectedItem() >= 0) {
					int selectedItem = buttons::getSelectedItem();
					if(selectedItem < SELECT_EQUIPMENT){
						drawItemInfoPanel(pc->getInventory()[selectedItem]);
					} else if(selectedItem < SELECT_WEAPON) {
						if(pc->getEquipment()[selectedItem - SELECT_EQUIPMENT] > 0) drawItemInfoPanel(items::getItem(pc->getEquipment()[selectedItem - SELECT_EQUIPMENT]));
					} else if(selectedItem == SELECT_WEAPON + WEAPONSPACE) {
						drawItemInfoPanel(pc->getEquippedWeapon());
					} else if(selectedItem < SELECT_GROUND) {
						if(weapons[selectedItem - SELECT_WEAPON] > 0) drawItemInfoPanel(weapons[selectedItem - SELECT_WEAPON]);
					}
				}
			}
			
			if(_view == VIEW_GROUND_ITEMS || (_view == VIEW_INVENTORY && world::countItemsAt(pc->getX(), pc->getY()) > 0)) {
				const std::vector<ItemOrWeapon*> pile = world::getItemsAt(pc->getX(), pc->getY());
				
				drawText(ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 - CHARHEIGHT, "\\BGround");
				
				for(int i=0; i<GROUNDSPACE && i<pile.size(); ++i) {
					sf::Sprite* s = pile[i + buttons::getGroundScrollOffset()]->getSprite();
					s->setPosition(ITEM_IMGSIZE*21/2, ITEM_IMGSIZE/2 * (3 + 3*i));
					windowTexture.draw(*s);
					if(s->getGlobalBounds().contains(sf::Vector2<float>(mouseX, mouseY))){
						tooltipText = pile[i + buttons::getGroundScrollOffset()]->getName();
					}
				}
				
				if(buttons::getSelectedItem() >= SELECT_GROUND) {
					drawItemInfoPanel(pile[buttons::getSelectedItem() - SELECT_GROUND]);
				}
			} else if(_view == VIEW_CHAR_STATUS) {
				windowTexture.draw(statusTextboxSprite);
				std::stringstream infopile; // infopile rhymes with monopoly
				if(viewedEnemy) {
					infopile << viewedEnemy->getName() << "\n";
					infopile << "Health: " << viewedEnemy->getHealth() << "/" << viewedEnemy->getMaxHealth() << "\n";
					infopile << "Attack: " << viewedEnemy->getAttackStrength() << "\\S" << viewedEnemy->getAttackType() << "\\c\n";
					infopile << "Speed: " << viewedEnemy->getSpeed() << "\nRange: " << viewedEnemy->getRange();
					infopile << "\n\nPercent damage taken:\n   \\S0    1    2    3    4    5 \\c\n";
					for(int i=0;i<6;++i) infopile << padInt(viewedEnemy->getDamageTaken((damage_type)i), 5);
					
					infopile << "\n";

					const vector<statusCondition>& statusConditions = viewedEnemy->getStatusEffects();
					for(int i=0; i<statusConditions.size(); ++i){
						infopile << status::names[statusConditions[i].id] << " (turns left: " << statusConditions[i].duration << ")\n";
					}
				} else {
					infopile << pc->getName() << "\n\nHealth: " << pc->getHealth() << "/" << pc->getMaxHealth() << "\nSpirit: " << pc->getSpirit() << "/" << pc->getMaxSpirit() << "\n\nSpeed: " << pc->stepsLeft() << "/" << pc->getSpeed() << "\nRange: " << pc->getRange() << "\n                \\S0    1    2    3    4    5 \\c\nDamage Taken:";
					for(int i=0;i<6;++i) infopile << padInt(pc->getDamageTaken((damage_type)i), 5);
					infopile << "\nDamage Dealt:";
					for(int i=0;i<6;++i) infopile << padInt(pc->getDamageDealt((damage_type)i), 5);

					infopile << "\n";

					const vector<statusCondition>& statusConditions = pc->getStatusEffects();
					for(int i=0; i<statusConditions.size(); ++i){
						infopile << status::names[statusConditions[i].id] << " (turns left: " << statusConditions[i].duration << ")\n";
					}
				}
				
				drawText(55,55,infopile.str());
			} else if(_view == VIEW_SPELL_DESCRIPTION) {
				if(buttons::getSelectedItem() >= 0){
					drawSpellInfoPanel(pc->getSpells()[buttons::getSelectedItem()]);
				}
			}
		}
		
		
		if(isDialogue) windowTexture.draw(dialogueTextboxSprite);
			
		const unordered_set<Button*>& visibleButtons = buttons::listButtons();
		for (auto itr = visibleButtons.begin(); itr != visibleButtons.end(); ++itr) windowTexture.draw(*(*itr)->getSprite());
		
		// need to draw this over buttons
		if(_view == VIEW_SPELL_LIST) { // not that, actually. rather, check what button is mouseovered and display the corresponding text
			PlayerCharacter* pc = characters::playable[buttons::getSelectedCharacter()];
			const std::vector<std::shared_ptr<spell>>& spells = pc->getSpells();
			for(int i=0; i<spells.size(); ++i) {
				std::string spellName = std::to_string(i+1) + ". ";
				if(spells[i]) {
					if(pc->canSpendSpirit(spells[i]->cost)) {
						spellName += "\\B" + spells[i]->name + "\n \\S]\\c" + std::to_string(spells[i]->cost * pc->getSpiritConsumption() / 100); // todo could put a heart icon if bloodstone conduit equipped
					} else {
						spellName += spells[i]->name + "\n \\S^\\c" + std::to_string(spells[i]->cost * pc->getSpiritConsumption() / 100);
					}
					if(mouseX < 190 && mouseY >= SCREENHEIGHT + 43 * (i-3) && mouseY <= SCREENHEIGHT + 43 * (i-2)  ) {
						drawSpellInfoPanel(pc->getSpells()[i]);
					}
				}
				drawText(6, (SCREENHEIGHT-43*3 + 4) + 43*i, spellName);
			}
		}
		
		if((_view == VIEW_INVENTORY || _view == VIEW_GROUND_ITEMS) && buttons::getSelectedItem() >= 0) {
			int selectedItem = buttons::getSelectedItem();
			int selectedItemX = 0;
			int selectedItemY = 0;
			if(selectedItem < SELECT_EQUIPMENT){
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * (selectedItem % INVENTORY_ROW);
				selectedItemY = ITEM_IMGSIZE * (selectedItem / INVENTORY_ROW + 2);
			} else if(selectedItem < SELECT_WEAPON) {
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * (selectedItem - SELECT_EQUIPMENT);
				selectedItemY = ITEM_IMGSIZE/2;
			} else if(selectedItem == SELECT_WEAPON + WEAPONSPACE) {
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * WEAPONSPACE;
				selectedItemY = ITEM_IMGSIZE * 13/2;
			} else if(selectedItem < SELECT_GROUND) {
				selectedItemX = ITEM_IMGSIZE/2 + ITEM_IMGSIZE * (selectedItem - SELECT_WEAPON);
				selectedItemY = ITEM_IMGSIZE * 13/2;
			} else {
				selectedItemX = ITEM_IMGSIZE * 21/2;
				selectedItemY = ITEM_IMGSIZE/2 * (3 + 3 * (selectedItem - SELECT_GROUND));
			}
			selectItemSprite.setPosition(selectedItemX, selectedItemY);
			windowTexture.draw(selectItemSprite);
		}
		
		// Various icons on player cards
		for(int i=0; i<characters::playable.size(); ++i){
			PlayerCharacter* pc = characters::playable[i];
			
			if(pc->getHealth() > 0){
				iconHealth.setPosition(SCREENWIDTH+6, 128*i+73);
				windowTexture.draw(iconHealth);
			}
			
			if(pc->getSpirit() > 0){
				iconSpirit.setPosition(SCREENWIDTH+5, 128*i+88);
				if(pc->getHealth() > 0) windowTexture.draw(iconSpirit);
			}
			
			if(pc->stepsLeft() > 0 || pc->getHealth() == 0){
				iconSpeed.setPosition(SCREENWIDTH+7, 128*i+106);
				windowTexture.draw(iconSpeed);
			}
			for(int j=0; j<pc->getSpeed(); ++j){
				sf::Sprite* speedTemp = &(j<pc->stepsLeft() ? speed1 : speed2);
				speedTemp->setPosition(SCREENWIDTH+21+j*8, 128*i+108);
				windowTexture.draw(*speedTemp);
			}
			
			if(!pc->hasActed()){
				iconAction.setPosition(SCREENWIDTH+108, 128*i+104);
				windowTexture.draw(iconAction);
			}
			
			const std::vector<statusCondition>& conditions = pc->getStatusEffects();
			for(int j=0; j<conditions.size(); ++j){
				buffIconSprite.setTextureRect(sf::IntRect(16*(conditions[j].id%16), 16*(conditions[j].id/16),16,16));
				buffIconSprite.setPosition(SCREENWIDTH + 71 + 17*(j%3), 128*i+7 + 17*(j/3));
				windowTexture.draw(buffIconSprite);
			}
		}
		
		for(auto itr=animations.begin(); itr!=animations.end();){
			sf::Sprite* s = (*itr)->update();
			if(s) {
				if((*itr)->atTile) s->setPosition(((*itr)->offsetX - offsetX) * TILESIZE, ((*itr)->offsetY - offsetY) * TILESIZE);
				windowTexture.draw(*s);
				++itr;
			}
			else { 
				delete *itr;
				itr = animations.erase(itr); 
			}
		}
		
		for(auto itr=projectiles.begin(); itr!=projectiles.end();){
			if((*itr)->isDone()) {
				itr = projectiles.erase(itr);
			}
			else {				
				sf::Sprite* s = (*itr)->getSprite();
				s->setPosition((*itr)->getCurrentX() - offsetX * TILESIZE, (*itr)->getCurrentY() - offsetY * TILESIZE);
				windowTexture.draw(*s);
				++itr;
			}
		}
		
		for(auto itr=damageTexts.begin(); itr!=damageTexts.end();){
			int now = timekeeping::currentTick();
			if(now > itr->endTick) {
				itr = damageTexts.erase(itr); 
			}
			else {
				std::string text = std::to_string(itr->amt);
				
				int baseX = (itr->x - offsetX)*TILESIZE + (TILESIZE - getEscapedWidth(text)*DMGCHARW)/2;
				charSprite.setTexture(dmgFontTexture);
				
				for(int i = 0; i < text.length(); ++i){
					if(text[i] == '-') {
						charSprite.setTextureRect(sf::IntRect(DMGCHARW*10, DMGCHARH, DMGCHARW, DMGCHARH));
					} else if (text[i] == '0' && text.length() == 1) {
						charSprite.setTextureRect(sf::IntRect(DMGCHARW*10, 0, DMGCHARW, DMGCHARH));
					} else {
						charSprite.setTextureRect(sf::IntRect((text[i]-'0')*DMGCHARW, DMGCHARH*(itr->amt < 0), DMGCHARW, DMGCHARH));
					}
					charSprite.setPosition(baseX + i*DMGCHARW, (itr->y-offsetY)*TILESIZE-30 + (itr->endTick- now)*3);
					charSprite.setPosition(baseX + i*DMGCHARW, (itr->y-offsetY)*TILESIZE-30 + (itr->endTick- now)*3);
					windowTexture.draw(charSprite);
				}
				++itr;
			}
		}
		
		/*if(buttons::getSelectedCharacter() == -1){
			sf::Text t("Select a character...", font, 24);
			t.setPosition(64*4, SCREENWIDTH+20);
			t.setFillColor(sf::Color::Black);
			windowTexture.draw(t);
		}*/
		
		if(isDialogue) {
			int textboxStyleOffset = 0;
			if(dialogueName.length() == 0) textboxStyleOffset = 318;
			else if(dialogueSpriteId < 0) textboxStyleOffset = 159;
			// TODO don't hardcode some of these numbers
			dialogueTextboxSprite.setTextureRect(sf::IntRect(0, textboxStyleOffset, 657, 159));

			if(dialogueSpriteId >= 0){
				int talkspritesWidth = talkspriteTexture.getSize().x / ITEM_IMGSIZE;
				dialogueFaceSprite.setTextureRect(sf::IntRect((dialogueSpriteId%talkspritesWidth)*ITEM_IMGSIZE, (dialogueSpriteId/talkspritesWidth)*ITEM_IMGSIZE, ITEM_IMGSIZE,ITEM_IMGSIZE));
				windowTexture.draw(dialogueFaceSprite);
				drawText(124,SCREENHEIGHT - 185, dialogueName);
				
				narrateText(124,SCREENHEIGHT - 158, dialogueText, dialogueSoundId, 48);
			} else if(dialogueName.length() > 0) {
				drawText(55,SCREENHEIGHT - 185, dialogueName);
				narrateText(55,SCREENHEIGHT - 158, dialogueText, dialogueSoundId, 53);
			} else {
				narrateText(55,SCREENHEIGHT - 185, dialogueText, dialogueSoundId, 53);
			}
		
			for(int i=0; i<dialogueResponseCount; ++i) {
				drawText(123, SCREENHEIGHT - 118 + 20*i, (*dialogueResponseOptions)[i]);
			}
			
			if(isSceneImage) {
				windowTexture.draw(sceneImageSprite);
			}
		}
		
		if(tooltipText.length() > 0){
			/*sf::Text tooltip(tooltipText, font, 20);
			//tooltip.setOutlineColor(sf::Color::Black);
			tooltip.setFillColor(sf::Color::Black);
			tooltip.setPosition(mouseX - (int)(tooltip.getLocalBounds().width), mouseY - (int)(tooltip.getLocalBounds().height));
			windowTexture.draw(tooltip); // TODO background for it*/
			
			int tooltipX = mouseX - getEscapedWidth(tooltipText)*CHARWIDTH;
			if(tooltipX < 0) tooltipX = 0;
			
			drawText(tooltipX, mouseY - CHARHEIGHT, tooltipText);
		
		}
		windowTexture.display();
		windowSprite.setTexture(windowTexture.getTexture());
		
		window.draw(windowSprite);
		window.display();
	}

	void cleanup(){
		delete tilesetTexture;
	}
}

#endif
