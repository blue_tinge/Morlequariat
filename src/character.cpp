#include "characters.h"
#include "animations.h"
#include "display.h"
#include "timekeeping.h"

//forward declaration
namespace cutscenes {
	void startDialogue(int);
}

namespace world {
	bool blockedOrOccupied(int, int);
	void damageAtTile(int, int, int, damage_type);
}

void StattedCharacter::beginTurn(){
		
	// Durations of status conditions tick down.
	// Remove any that are expiring.
	int read = 0;
	int write = 0;
	while(read < statusEffects.size()){
		if(statusEffects[read].duration == 0){
			if(!specificRemoveCondition(statusEffects[read])) removeCondition(statusEffects[read]);
		} else {
			statusEffects[write] = statusEffects[read];
			--statusEffects[write].duration;
			if(!specificConditionForTurn(statusEffects[write])) conditionForTurn(statusEffects[write]);
			++write;
		}
		++read;
	}	
	while(statusEffects.size() > write) statusEffects.pop_back();
	
	specificBeginTurn();
}

void StattedCharacter::takeDamage(int amt, damage_type dt){
	int delta = (amt * damageTaken[dt]) / 100;
	hp -= delta;
	display::addAnimationAtTile(x,y,animations::takeDamage+dt);
	display::showDamageText(delta, x,y/*, (animations::takeDamage+dt)->frames*3*/);
	if(hp <= 0) {
		hp = 0;
		onDeath();
	}
	if(hp > hpMax) hp = hpMax;
}

int StattedCharacter::hasStatusEffect(int id) const {
	for(int i=0; i<statusEffects.size(); ++i) {
		if(statusEffects[i].id == id) return statusEffects[i].intensity > 0 ? statusEffects[i].intensity : true;
	}
	return false;
}

int StattedCharacter::totalStatusEffectIntensity(int id) const {
	int total = 0;
	for(int i=0; i<statusEffects.size(); ++i) {
		if(statusEffects[i].id == id) total += statusEffects[i].intensity;
	}
	return total;
}

		
void StattedCharacter::heal(int h) {
	display::showDamageText(-h, getX(), getY());
	if(hp+h>hpMax) hp=hpMax; else hp+=h;
}

void StattedCharacter::addCondition(conditionId id, int intensity, int duration) {
	if(intensity == 0) { // status condition is intensity-independent, so it might be possible to
						 // lengthen an existing condition
		for(int i=0; i<statusEffects.size(); ++i) {
			if(statusEffects[i].id == id) {
				// negative duration denotes unlimited
				if (statusEffects[i].duration > 0) statusEffects[i].duration += duration;
				return;
			}
		}
	} else if(intensity == 0 || duration < 0) { // status condition has unlimited duration, so it might
												// be possible to strengthen an existing condition
		for(int i=0; i<statusEffects.size(); ++i) {
			if(statusEffects[i].id == id) {
				// negative duration denotes unlimited
				statusEffects[i].intensity += intensity;
				return;
			}
		}
	}
	
	switch(id){
		case poisoned:
			display::addAnimationAtTile(x,y,animations::takeDamage+damage_type::poison); break;
		case spicy:
			display::addAnimationAtTile(x,y,animations::takeDamage+damage_type::fire); break;			
		case minty:
			display::addAnimationAtTile(x,y,animations::takeDamage+damage_type::frost); break;
		case levit:
			display::addAnimationAtTile(x,y,&animations::airBuff); break;
		
		
		case spdDn: speed -= intensity; break;
		case phDefDn:
		case hlDefDn:
		case fiDefDn:
		case fsDefDn:
		case pnDefDn:
		case crDefDn:
			damageTaken[id - conditionId::phDefDn] += intensity; break;
			
		case spdUp: speed += intensity; break;
		case phDefUp:
		case hlDefUp:
		case fiDefUp:
		case fsDefUp:
		case pnDefUp:
		case crDefUp:
			damageTaken[id - conditionId::phDefUp] -= intensity; break;
	}
	
	statusCondition s;
	s.id = id;
	s.intensity = intensity;
	s.duration = duration;
	specificAddCondition(s);
	statusEffects.push_back(s);
}

void StattedCharacter::conditionForTurn(statusCondition& sc){
	switch(sc.id){
		case poisoned:	takeDamage(sc.intensity, damage_type::poison); break;
		case burning:   takeDamage(sc.intensity, damage_type::fire); --sc.intensity; break;
		case cursed:    takeDamage(sc.intensity, damage_type::corrupt); ++sc.intensity; break;
		case regen:     heal(sc.intensity); break;
	}
}

void StattedCharacter::removeCondition(statusCondition& sc){
	switch(sc.id){
		case spdDn: speed += sc.intensity; break;
		case phDefDn:
		case hlDefDn:
		case fiDefDn:
		case fsDefDn:
		case pnDefDn:
		case crDefDn:
			damageTaken[sc.id - conditionId::phDefDn] -= sc.intensity; break;
			
		case spdUp: speed -= sc.intensity; break;
		case phDefUp:
		case hlDefUp:
		case fiDefUp:
		case fsDefUp:
		case pnDefUp:
		case crDefUp:
			damageTaken[sc.id - conditionId::phDefUp] += sc.intensity; break;
	}
}

void StattedCharacter::onPush(direction dir, int collisionDamage) {
	int newX = x;
	int newY = y;
	
	switch(dir) {
		case _up:    newY -= 1; break;
		case _down:  newY += 1; break;
		case _left:  newX -= 1; break;
		case _right: newX += 1; break;
	}
	
	if(world::blockedOrOccupied(newX, newY)) {
		takeDamage(collisionDamage, damage_type::physical);
		world::damageAtTile(newX, newY, collisionDamage, damage_type::physical);
	} else {
		moveTo(newX, newY);
	}
	
}

characters::animation* characters::createLinearAnimation(int textureOffsetX, int textureOffsetY, int frames, int deltaX, int deltaY, int width, int height, animation* next, int durationMultiplier){
	animation* a = new animation;
	a->frames = frames;
	a->frameRects = new sf::IntRect[frames];
	a->offsets = new sf::Vector2i[frames];
	for(int i=0; i<frames;++i){
		a->frameRects[i] = sf::IntRect(textureOffsetX + i*width, textureOffsetY, width, height);
		a->offsets[i] = sf::Vector2i(deltaX*i/frames, deltaY*i/frames);
	}
	a->next = next ? next : a; // animations loop by default
	a->frameDuration = durationMultiplier;
	return a;
}

void Character::updateAnimation(){
	int now = timekeeping::currentTick();
	int frameIndex = (now - animationStart) / (FRAME_DURATION*currentAnimation->frameDuration);
	if(frameIndex >= currentAnimation->frames) {
		currentAnimation = currentAnimation->next;
		animationStart = now;
		frameIndex = 0;
	}
	sf::IntRect ir = currentAnimation->frameRects[frameIndex];
	sprite.setTextureRect(ir);
	sf::Vector2i offset = currentAnimation->offsets[frameIndex];
	pixelOffsetX = offset.x;
	pixelOffsetY = offset.y;
}

void Character::setAnimation(characters::animation* a, bool reset){
	if(!reset && currentAnimation == a) return; // don't reset the animation
	currentAnimation = a;
	animationStart = timekeeping::currentTick();
}

NPC::NPC(int _id, int _ds) {
	
	id = _id;
	dialogueStart = _ds;
	
	tex.loadFromFile("img/npc/"+std::to_string(_id)+".png");
	sprite.setTexture(tex);
	
	//currentAnimation = characters::createLinearAnimation(0,0,tex.getSize().x/48,0,0,48,56);	
	
	for(int i=0;i<4;++i){
		idle[i] = characters::createLinearAnimation(0,56*i,4,0,0,48,56, NULL, 4);
	}
	walk[0] = characters::createLinearAnimation(192,0,   4,-48,  0,48,56,idle[0]);
	walk[1] = characters::createLinearAnimation(192,56,  4,  0,-48,48,56,idle[1]);
	walk[2] = characters::createLinearAnimation(192,56*2,4, 48,  0,48,56,idle[2]);
	walk[3] = characters::createLinearAnimation(192,56*3,4,  0, 48,48,56,idle[3]);
	
	currentAnimation = idle[0];
}

void NPC::startDialogue() const {
	cutscenes::startDialogue(dialogueStart);
}

void NPC::takeSteps(direction dir, int steps) {
	if(steps == 0) return;
	
	setAnimation(walk[dir], true);
	
	int deltax = 0;
	int deltay = 0;
	switch(dir) {
		case _up:    deltay -= 1; break;
		case _down:  deltay += 1; break;
		case _left:  deltax -= 1; break;
		case _right: deltax += 1; break;
	}
	
	timekeeping::schedule([this, deltax, deltay, dir, steps](){
		setXY(x + deltax, y + deltay);
		takeSteps(dir, steps-1);
	}, walk[dir]->frames*FRAME_DURATION - 1);
}

